/*
 * Copyright (c) 2015 Newshunt. All rights reserved.
 */
package com.bheemesh.myapplication.entities;

/**
 * Handling the error from the retrofit
 *
 * @author bheemesh on 7/20/2015.
 */
public enum StatusError {
    NETWORK_ERROR("network_error"),
    CONVERSION_ERROR("conversion_error"),
    HTTP_ERROR("http_error"),
    UNEXPECTED_ERROR("unexpected_error"),
    NO_CONTENT_ERROR("noContent");

    private String name;

    StatusError(String name) {
        this.name = name;
    }

    public static StatusError fromName(String name) {
        for (StatusError type : StatusError.values()) {
            if (type.name.equalsIgnoreCase(name)) {
                return type;
            }
        }
        return UNEXPECTED_ERROR;
    }

    public String getName() {
        return name;
    }
}
