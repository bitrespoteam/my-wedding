package com.bheemesh.myapplication.entities;

/**
 * Created by bheemesh on 4/1/16.
 */
public enum RequestActionType {
    REGISTER(200, "register"),
    LOGIN(102, "login"),
    USER_TOKEN(101, "user_token"),
    SELF_FEED(103, "self_feed"),
    ALL_NEWS(104, "all_news"),
    POST_NEWS(105, "post_news"),
    AGREE_NEWS(106, "agree_news"),
    DIS_AGREE_NEWS(107, "dis_agree_news");

    private int index;
    private String name;

    RequestActionType(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public static RequestActionType fromName(String name) {
        for (RequestActionType type : RequestActionType.values()) {
            if (type.name.equalsIgnoreCase(name)) {
                return type;
            }
        }
        return REGISTER;
    }

    public static RequestActionType fromIndex(int index) {
        for (RequestActionType type : RequestActionType.values()) {
            if (type.index == index) {
                return type;
            }
        }
        return REGISTER;
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }
}
