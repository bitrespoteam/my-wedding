package com.bheemesh.myapplication.entities;

/**
 * Created by bheemesh on 2/1/16.
 */
public enum HomeDisplayCardType {
    NORMAL(200, "normal"),
    VENDOR_LIST_DETAIL(201, "vendor_list_detail");

    private int index;
    private String name;

    HomeDisplayCardType(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public static HomeDisplayCardType fromName(String name) {
        for (HomeDisplayCardType type : HomeDisplayCardType.values()) {
            if (type.name.equalsIgnoreCase(name)) {
                return type;
            }
        }
        return NORMAL;
    }

    public static HomeDisplayCardType fromIndex(int index) {
        for (HomeDisplayCardType type : HomeDisplayCardType.values()) {
            if (type.index == index) {
                return type;
            }
        }
        return NORMAL;
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }
}
