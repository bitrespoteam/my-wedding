package com.bheemesh.myapplication.entities;

/**
 * Created by bheemesh on 2/1/16.
 */
public enum VendorType {
    VENNUE(200, "VENNUE"),
    PHOTO_GRAPHERS(201, "PHOTO_GRAPHERS"),
    MAKE_UP_ARTIST(202, "MAKE_UP_ARTIST"),
    BRIDAL_WEAR(203, "BRIDAL_WEAR"),
    GROOM_WEAR(204, "GROOM_WEAR"),
    DECORATOR(205, "DECORATOR"),
    WEDDING_PLANNER(206, "WEDDING_PLANNER"),
    INVITATION(207, "INVITATION"),
    CINEMA_OR_VIDEO(208, "CINEMA_OR_VIDEO"),
    MEHENDI_ARTIST(209, "MEHENDI_ARTIST"),
    CAKE(210, "CAKE"),
    JEWELLERY(211, "JEWELLERY"),
    CATERING_SERVICES(212, "CATERING_SERVICES"),
    DJs(213, "DJs"),
    CHOREOGRAPHER(214, "CHOREOGRAPHER"),
    ACCESSORIES(215, "ACCESSORIES");



    private int index;
    private String name;

    VendorType(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public static VendorType fromName(String name) {
        for (VendorType type : VendorType.values()) {
            if (type.name.equalsIgnoreCase(name)) {
                return type;
            }
        }
        return VENNUE;
    }

    public static VendorType fromIndex(int index) {
        for (VendorType type : VendorType.values()) {
            if (type.index == index) {
                return type;
            }
        }
        return VENNUE;
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }

    public static String[] getNames() {
        VendorType[] states = values();
        String[] names = new String[states.length];

        for (int i = 0; i < states.length; i++) {
            names[i] = states[i].name();
        }

        return names;
    }
}
