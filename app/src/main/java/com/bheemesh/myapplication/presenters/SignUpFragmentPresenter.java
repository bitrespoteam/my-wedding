package com.bheemesh.myapplication.presenters;

import com.bheemesh.myapplication.Utils.Constants;
import com.bheemesh.myapplication.Utils.PostObjects;
import com.bheemesh.myapplication.beans.ApiResponse;
import com.bheemesh.myapplication.beans.DataStatus;
import com.bheemesh.myapplication.beans.UserDetails;
import com.bheemesh.myapplication.entities.RequestActionType;
import com.bheemesh.myapplication.fragments.SignUpFragment;
import com.bheemesh.myapplication.http.HttpEngine;
import com.bheemesh.myapplication.listeners.HttpListener;
import com.bheemesh.myapplication.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Created on 26/1/16.
 *
 * @author bheemesh
 */
public class SignUpFragmentPresenter implements HttpListener {
    SignUpFragment fragmentInstance;
    RequestActionType requestActionType;
    UserDetails userDetails;

    public SignUpFragmentPresenter(SignUpFragment fragment) {
        this.fragmentInstance = fragment;
    }

    public void signUpUserToServer(UserDetails userDetails, RequestActionType requestActionType){
        this.userDetails = userDetails;
        String postBody = PostObjects.createRegisterPostBody(userDetails).toString();
        HttpRequest httpRequest = new HttpRequest(Constants.REGISTER_URL, postBody, this);
        httpRequest.headerParams = true;
        HttpEngine.getInstance().addRequest(httpRequest);
    }

    @Override
    public void handleHttpResponse(HttpRequest httpRequest) {
        System.out.println("NEWSNIT : SignUpFragmentPresenter : handleHttpResponse");
        if (httpRequest == null || httpRequest.content == null) {
            System.out.println("BHEEM : Present in the handleHttpResponse : Empty response");
            sendErrorResponse();
        } else {
            InputStream is = new ByteArrayInputStream(httpRequest.content);
            Reader reader = new InputStreamReader(is);
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.enableComplexMapKeySerialization().create();
            ApiResponse<DataStatus> response = gson.fromJson(reader, new
                    TypeToken<ApiResponse<DataStatus>>() {
                    }.getType());
            fragmentInstance.onLoginResult(userDetails, response);
        }
    }

    @Override
    public void handleHttpException(Exception e, HttpRequest httpRequest) {
        System.out.println("NEWSNIT : SignUpFragmentPresenter : handleHttpResponse");
        sendErrorResponse();
    }

    private void sendErrorResponse(){
        ApiResponse<DataStatus> response = new ApiResponse<>();
        response.setStatus("500");

        DataStatus dataStatus = new DataStatus();
        dataStatus.setMessage("Sorry Unable to register user.");
        response.setData(dataStatus);

        fragmentInstance.onLoginResult(userDetails,response);
    }
}
