package com.bheemesh.myapplication.presenters;

import com.bheemesh.myapplication.beans.BaseItem;
import com.bheemesh.myapplication.entities.RequestActionType;
import com.bheemesh.myapplication.fragments.DetailsFragment;
import com.bheemesh.myapplication.http.HttpRequest;
import com.bheemesh.myapplication.listeners.HttpListener;

/**
 * Created on 26/1/16.
 *
 * @author bheemesh
 */
public class DetailsFragmentPresenter implements HttpListener{
    DetailsFragment fragmentInstance;
    RequestActionType requestActionType;

    public DetailsFragmentPresenter(DetailsFragment fragment) {
        this.fragmentInstance = fragment;
    }

    public void sendAgreeDisAgreeVote(BaseItem item, RequestActionType requestActionType) {
        this.requestActionType = requestActionType;
    }

    @Override
    public void handleHttpResponse(HttpRequest httpRequest) {
        System.out.println("NEWSNIT : SignUpFragmentPresenter : handleHttpResponse"+new String
                (httpRequest.content));
    }

    @Override
    public void handleHttpException(Exception e, HttpRequest httpRequest) {
        System.out.println("NEWSNIT : SignUpFragmentPresenter : handleHttpResponse");
        sendErrorResponse();
    }

    private void sendErrorResponse(){

    }
}
