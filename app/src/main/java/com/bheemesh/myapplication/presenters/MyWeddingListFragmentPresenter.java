package com.bheemesh.myapplication.presenters;

import com.bheemesh.myapplication.R;
import com.bheemesh.myapplication.beans.BaseItem;
import com.bheemesh.myapplication.beans.BaseItemContainer;
import com.bheemesh.myapplication.beans.Reviews;
import com.bheemesh.myapplication.beans.VendorPlans;
import com.bheemesh.myapplication.beans.VendorSeats;
import com.bheemesh.myapplication.beans.WeddingVendor;
import com.bheemesh.myapplication.entities.HomeDisplayCardType;
import com.bheemesh.myapplication.entities.RequestActionType;
import com.bheemesh.myapplication.entities.VendorType;
import com.bheemesh.myapplication.fragments.MyWeddingListFragment;
import com.bheemesh.myapplication.listeners.HttpListener;
import com.bheemesh.myapplication.http.HttpRequest;

import java.util.ArrayList;

/**
 * Created on 26/1/16.
 *
 * @author bheemesh
 */
public class MyWeddingListFragmentPresenter implements HttpListener {
    MyWeddingListFragment frag;
    RequestActionType requestActionType;

    public MyWeddingListFragmentPresenter(MyWeddingListFragment frag) {
        this.frag = frag;
    }

    private void updateBaseItems(ArrayList<BaseItem> baseItems) {
        frag.vendorsContainer = new BaseItemContainer();
        frag.vendorsContainer.setId("" + frag.pageTabName);
        frag.vendorsContainer.setCollectionTitle(frag.pageTabName + " Title");
        frag.vendorsContainer.setCollectionSubTitle(frag.pageTabName + " Sub Title");
        frag.vendorsContainer.setDisplyName("" + frag.pageTabName);
        frag.vendorsContainer.setBaseItems(baseItems);
        frag.initUIAdapter();
    }

    public void loadBaseItemInDB() {
        ArrayList<BaseItem> vendorList = new ArrayList<>();
        String[] name = frag.getActivity().getResources().getStringArray(R.array.vendor_name);
        String[] city = frag.getActivity().getResources().getStringArray(R.array.vendor_city);
        String[] phone = frag.getActivity().getResources().getStringArray(R.array.vendor_phone);
        String[] email = frag.getActivity().getResources().getStringArray(R.array.vendor_email);

        BaseItem baseItem = null;
        for (int i = 0; i < name.length; i++) {
            baseItem = new BaseItem();
            baseItem.setId("" + i);
            baseItem.setName(name[i]);
            baseItem.setCity(city[i]);
            baseItem.setLocationId(city[i]);
            baseItem.setNumberLikes(""+i);
            baseItem.setRating(""+4);
            baseItem.setRatingTotal(""+5);
            baseItem.setPhone(phone[i]);
            baseItem.setEmail(email[i]);
            baseItem.setPlans(getPlans());
            baseItem.setArea(getSeatPlans());
            baseItem.setReviews(getReviews());
            baseItem.setDisplayCardType(HomeDisplayCardType.NORMAL);
            baseItem.setVendorType(VendorType.VENNUE);
            vendorList.add(baseItem);
        }
        updateBaseItems(vendorList);
    }

    private ArrayList<VendorPlans> getPlans(){
        ArrayList<VendorPlans> planList = new ArrayList<VendorPlans>();
        VendorPlans plan = null;
        for (int i = 0; i < 2; i++) {
            plan = new VendorPlans();
            plan.setTitle("Plan "+i);
            plan.setDescription("Plan "+i+" Description");
            planList.add(plan);
        }
        return planList;
    }

    private ArrayList<VendorSeats> getSeatPlans(){
        ArrayList<VendorSeats> planList = new ArrayList<VendorSeats>();
        VendorSeats plan = null;
        for (int i = 0; i < 2; i++) {
            plan = new VendorSeats();
            plan.setTitle("Taj Hotel : "+i);
            plan.setFloating_seats("Floating seats");
            plan.setSitting_seats("Sitting seats");
            plan.setVendor_id("Vendor id");
            planList.add(plan);
        }
        return planList;
    }

    private ArrayList<Reviews> getReviews(){
        ArrayList<Reviews> planList = new ArrayList<Reviews>();
        Reviews review = null;
        for (int i = 0; i < 2; i++) {
            review = new Reviews();
            review.setTitle("Awesome : "+i);
            review.setDescription("Good place for marraiage : "+i);
            review.setAuthorName("Authod");
            review.setDate("Aug 15 2016");

            review.setRating("3");
            review.setRating_total("5");
            review.setVendorId("no id");
            planList.add(review);
        }
        return planList;
    }


    public void requestToServer(RequestActionType requestActionType) {
        this.requestActionType = requestActionType;
    }

    @Override
    public void handleHttpResponse(HttpRequest httpRequest) {
    }

    @Override
    public void handleHttpException(Exception e, HttpRequest httpRequest) {
    }

    public ArrayList<WeddingVendor> loadVendorsTypes() {
        ArrayList<WeddingVendor> weddingVendorList = new ArrayList<>();
        String[] vendorTypes = VendorType.getNames();
        WeddingVendor vendor = null;
        for (int i = 0; i < vendorTypes.length; i++) {
            vendor = new WeddingVendor();
            vendor.setTitle(vendorTypes[i]);
            vendor.setId(vendorTypes[i]);
        }
        return weddingVendorList;
    }
}
