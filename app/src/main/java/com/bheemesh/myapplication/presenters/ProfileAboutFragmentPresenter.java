package com.bheemesh.myapplication.presenters;

import com.bheemesh.myapplication.beans.BaseItem;
import com.bheemesh.myapplication.fragments.DetailsFragment;

/**
 * Created on 26/1/16.
 *
 * @author bheemesh
 */
public class ProfileAboutFragmentPresenter {
    DetailsFragment fragmentInstance;

    public ProfileAboutFragmentPresenter(DetailsFragment fragment) {
        this.fragmentInstance = fragment;
    }

    public void updateRecentDB(BaseItem item) {
        
    }
}
