package com.bheemesh.myapplication.presenters;

import com.bheemesh.myapplication.R;
import com.bheemesh.myapplication.Utils.Constants;
import com.bheemesh.myapplication.Utils.PostObjects;
import com.bheemesh.myapplication.activities.SplashViewActivity;
import com.bheemesh.myapplication.beans.BaseItem;
import com.bheemesh.myapplication.beans.LocationItem;
import com.bheemesh.myapplication.beans.Reviews;
import com.bheemesh.myapplication.beans.VendorPlans;
import com.bheemesh.myapplication.beans.VendorSeats;
import com.bheemesh.myapplication.entities.HomeDisplayCardType;
import com.bheemesh.myapplication.entities.RequestActionType;
import com.bheemesh.myapplication.entities.VendorType;
import com.bheemesh.myapplication.http.HttpEngine;
import com.bheemesh.myapplication.http.HttpRequest;
import com.bheemesh.myapplication.listeners.HttpListener;
import com.bheemesh.myapplication.singleton.SSO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created on 26/1/16.
 *
 * @author bheemesh
 */
public class SplashActivityPresenter implements HttpListener{
    private SplashViewActivity activity;
    private RequestActionType requestActionType;

    public SplashActivityPresenter(SplashViewActivity activity) {
        this.activity = activity;
    }

    public void initDB() {
        loadBaseItemInDB();
        SSO.getInstance();
    }

    public void registerToServer(RequestActionType requestActionType) {
        this.requestActionType = requestActionType;
        String postBody = PostObjects.createLoginPostBody(null).toString();
        Map<String, String> headers  = new HashMap<String, String>();
        headers.put("Authorization", "Token "+ SSO.getUserToken());
        HttpEngine.getInstance().addHeaderValues(headers);

        HttpRequest httpRequest = new HttpRequest(Constants.LOGIN_URL, postBody, this);
        httpRequest.headerParams = true;
        HttpEngine.getInstance().addRequest(httpRequest);
    }

//    private VendorType vendorType;
//    private String name;
//    private String city;
//    private String locationId;
//    private String numberLikes;
//    private String rating;
//    private String ratingTotal;
//    private String phone;
//    private String email;
//    private List<VendorPlans> plans;
//    private List<VendorSeats> area;
//    private List<String> imageUrl;
//    private List<Reviews> reviews;
//    private HomeDisplayCardType displayCardType;

    private void loadBaseItemInDB() {
        ArrayList<BaseItem> vendorList = new ArrayList<>();
        String[] name = activity.getResources().getStringArray(R.array.vendor_name);
        String[] city = activity.getResources().getStringArray(R.array.vendor_city);
        String[] phone = activity.getResources().getStringArray(R.array.vendor_phone);
        String[] email = activity.getResources().getStringArray(R.array.vendor_email);

        BaseItem baseItem = null;
        for (int i = 0; i < name.length; i++) {
            baseItem = new BaseItem();
            baseItem.setId("" + i);
            baseItem.setName(name[i]);
            baseItem.setCity(city[i]);
            baseItem.setLocationId(city[i]);
            baseItem.setNumberLikes(""+i);
            baseItem.setRating(""+4);
            baseItem.setRatingTotal(""+5);
            baseItem.setPhone(phone[i]);
            baseItem.setEmail(email[i]);
            baseItem.setPlans(getPlans());
            baseItem.setArea(getSeatPlans());
            baseItem.setReviews(getReviews());
            baseItem.setDisplayCardType(HomeDisplayCardType.NORMAL);
            baseItem.setVendorType(VendorType.VENNUE);
            vendorList.add(baseItem);
        }
    }

    private ArrayList<VendorPlans> getPlans(){
        ArrayList<VendorPlans> planList = new ArrayList<VendorPlans>();
        VendorPlans plan = null;
        for (int i = 0; i < 2; i++) {
            plan = new VendorPlans();
            plan.setTitle("Plan "+i);
            plan.setDescription("Plan "+i+" Description");
            planList.add(plan);
        }
        return planList;
    }

    private ArrayList<VendorSeats> getSeatPlans(){
        ArrayList<VendorSeats> planList = new ArrayList<VendorSeats>();
        VendorSeats plan = null;
        for (int i = 0; i < 2; i++) {
            plan = new VendorSeats();
            plan.setTitle("Taj Hotel : "+i);
            plan.setFloating_seats("Floating seats");
            plan.setSitting_seats("Sitting seats");
            plan.setVendor_id("Vendor id");
            planList.add(plan);
        }
        return planList;
    }

    private ArrayList<Reviews> getReviews(){
        ArrayList<Reviews> planList = new ArrayList<Reviews>();
        Reviews review = null;
        for (int i = 0; i < 2; i++) {
            review = new Reviews();
            review.setTitle("Awesome : "+i);
            review.setDescription("Good place for marraiage : "+i);
            review.setAuthorName("Authod");
            review.setDate("Aug 15 2016");

            review.setRating("3");
            review.setRating_total("5");
            review.setVendorId("no id");
            planList.add(review);
        }
        return planList;
    }



    private LocationItem getLocation(int index) {
        LocationItem locationItem = new LocationItem();
        locationItem.setCountry("India");
        locationItem.setState("Karnataka");
        locationItem.setDistrict("Shimoga");
        locationItem.setTaluk("Honnali");
        if (index % 2 == 0) {
            locationItem.setLatitude("12.5332");
            locationItem.setLongitude("76.3434");
        } else {
            locationItem.setLatitude("12.5345");
            locationItem.setLongitude("76.3432");
        }

        locationItem.setLandMark("Near tunga bridge");
        return locationItem;
    }

    @Override
    public void handleHttpResponse(HttpRequest httpRequest) {
        System.out.println("BHEEM : Present in the handleHttpResponse : " + requestActionType
                .getName());

        if (httpRequest == null || httpRequest.content == null) {
            System.out.println("BHEEM : Present in the handleHttpResponse : Empty response");
        } else {
            System.out.println("BHEEM : Present in the handleHttpResponse : Got response : " + new
                    String(httpRequest.content));
        }
    }

    @Override
    public void handleHttpException(Exception e, HttpRequest httpRequest) {
        System.out.println("BHEEM : Present in the handleHttpException : " + e.getMessage() + " " +
                requestActionType.getName());
    }
}
