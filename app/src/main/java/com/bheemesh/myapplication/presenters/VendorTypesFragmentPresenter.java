package com.bheemesh.myapplication.presenters;

import com.bheemesh.myapplication.R;
import com.bheemesh.myapplication.beans.BaseItem;
import com.bheemesh.myapplication.beans.BaseItemContainer;
import com.bheemesh.myapplication.beans.Reviews;
import com.bheemesh.myapplication.beans.VendorPlans;
import com.bheemesh.myapplication.beans.VendorSeats;
import com.bheemesh.myapplication.beans.WeddingVendor;
import com.bheemesh.myapplication.entities.HomeDisplayCardType;
import com.bheemesh.myapplication.entities.RequestActionType;
import com.bheemesh.myapplication.entities.VendorType;
import com.bheemesh.myapplication.fragments.MyWeddingListFragment;
import com.bheemesh.myapplication.fragments.VendorTypesFragment;
import com.bheemesh.myapplication.http.HttpRequest;
import com.bheemesh.myapplication.listeners.HttpListener;

import java.util.ArrayList;

/**
 * Created on 26/1/16.
 *
 * @author bheemesh
 */
public class VendorTypesFragmentPresenter implements HttpListener {
    VendorTypesFragment frag;
    RequestActionType requestActionType;

    public VendorTypesFragmentPresenter(VendorTypesFragment frag) {
        this.frag = frag;
    }

    public void requestToServer(RequestActionType requestActionType) {
        this.requestActionType = requestActionType;
    }

    @Override
    public void handleHttpResponse(HttpRequest httpRequest) {
    }

    @Override
    public void handleHttpException(Exception e, HttpRequest httpRequest) {
    }

    public ArrayList<WeddingVendor> loadVendorsTypes() {
        ArrayList<WeddingVendor> weddingVendorList = new ArrayList<>();
        String[] vendorTypes = VendorType.getNames();
        WeddingVendor vendor = null;
        for (int i = 0; i < vendorTypes.length; i++) {
            vendor = new WeddingVendor();
            vendor.setTitle(vendorTypes[i]);
            vendor.setId(vendorTypes[i]);
            weddingVendorList.add(vendor);
        }
        return weddingVendorList;
    }
}
