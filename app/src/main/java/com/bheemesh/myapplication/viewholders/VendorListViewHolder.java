package com.bheemesh.myapplication.viewholders;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bheemesh.myapplication.R;
import com.bheemesh.myapplication.beans.BaseItem;
import com.bheemesh.myapplication.beans.Entity;
import com.bheemesh.myapplication.beans.VendorPlans;
import com.bheemesh.myapplication.entities.HomeActionType;
import com.bheemesh.myapplication.listeners.NewsUpdatableViewHolder;
import com.bheemesh.myapplication.listeners.OnItemClickListener;

/**
 * Created by bheemesh on 2/1/16.
 */
public class VendorListViewHolder extends RecyclerView.ViewHolder implements NewsUpdatableViewHolder{
    CardView cardItemLayout;
    private TextView name;
    private TextView city;
    private TextView plan;
    private TextView review;
    private ImageView vendorImage;
    OnItemClickListener clickListener;
    BaseItem vendorObj;

    public VendorListViewHolder(View itemView, OnItemClickListener clickListener) {
        super(itemView);
        this.clickListener = clickListener;
        cardItemLayout = (CardView) itemView.findViewById(R.id.cardlist_item);
        name = (TextView) itemView.findViewById(R.id.txt_vendor_name);
        city = (TextView) itemView.findViewById(R.id.txt_vendor_city);
        plan = (TextView) itemView.findViewById(R.id.txt_vendor_plan);
        review = (TextView) itemView.findViewById(R.id.txt_vendor_review);
        vendorImage = (ImageView) itemView.findViewById(R.id.iv_vendor_image);
        setOnclickListener(null);
    }

    private void setOnclickListener(final OnItemClickListener clickListener) {
        cardItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("card item got clicked");
                clickListener.onItemClick(view, vendorObj, HomeActionType.DETAILS);
            }
        });

    }

    @Override
    public void updateViewHolder(Context context, Entity vendor) {
        this.vendorObj = (BaseItem) vendor;
        name.setText(vendorObj.getName());
        city.setText(vendorObj.getCity());
        setPlanText();
        setReview();
        setOnclickListener(clickListener);
    }

    private void setPlanText(){
        if(vendorObj != null && vendorObj.getPlans() != null && vendorObj.getPlans().size() > 0 ){
            VendorPlans firstPlan = vendorObj.getPlans().get(0);
            plan.setText(firstPlan.getDescription());
        }
    }

    private void setReview(){
        if(vendorObj != null && vendorObj.getReviews() != null && vendorObj.getReviews().size() > 0 ){
            plan.setText(vendorObj.getReviews().size() + " Reviews");
        } else {
            review.setText("No one reviewed yet");
        }
    }
}
