package com.bheemesh.myapplication.viewholders;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bheemesh.myapplication.R;
import com.bheemesh.myapplication.beans.BaseItem;
import com.bheemesh.myapplication.beans.Entity;
import com.bheemesh.myapplication.beans.WeddingVendor;
import com.bheemesh.myapplication.entities.HomeActionType;
import com.bheemesh.myapplication.listeners.NewsUpdatableViewHolder;
import com.bheemesh.myapplication.listeners.OnItemClickListener;

/**
 * Created by bheemesh on 2/1/16.
 */
public class VendorNormalViewHolder extends RecyclerView.ViewHolder implements NewsUpdatableViewHolder{
    CardView cardItemLayout;
    ImageView isFavouriteImg;
    TextView title;
    TextView subTitle;
    OnItemClickListener clickListener;
    BaseItem vendorObj;

    public VendorNormalViewHolder(View itemView, OnItemClickListener clickListener) {
        super(itemView);
        this.clickListener = clickListener;
        cardItemLayout = (CardView) itemView.findViewById(R.id.cardlist_item);
        title = (TextView) itemView.findViewById(R.id.listitem_name);
        subTitle = (TextView) itemView.findViewById(R.id.listitem_subname);
        isFavouriteImg = (ImageView) itemView.findViewById(R.id.iv_favourite);
        setOnclickListener(null);
    }

    private void setOnclickListener(final OnItemClickListener clickListener) {
        cardItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("card item got clicked");
                clickListener.onItemClick(view, vendorObj, HomeActionType.DETAILS);
            }
        });

    }

    @Override
    public void updateViewHolder(Context context, Entity vendor) {
        this.vendorObj = (BaseItem) vendor;
        title.setText(vendorObj.getName());
        subTitle.setText(vendorObj.getCity());
        setOnclickListener(clickListener);
    }
}
