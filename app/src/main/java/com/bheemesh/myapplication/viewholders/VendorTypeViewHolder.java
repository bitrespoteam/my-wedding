package com.bheemesh.myapplication.viewholders;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bheemesh.myapplication.R;
import com.bheemesh.myapplication.beans.WeddingVendor;
import com.bheemesh.myapplication.entities.HomeActionType;
import com.bheemesh.myapplication.listeners.OnItemClickListener;

/**
 * Created by bheemesh on 2/1/16.
 */
public class VendorTypeViewHolder extends RecyclerView.ViewHolder {
    LinearLayout item_container;
    TextView title;
    ImageView vendorIcon;
    OnItemClickListener clickListener;

    WeddingVendor vendorObj;

    public VendorTypeViewHolder(View itemView, OnItemClickListener clickListener) {
        super(itemView);
        this.clickListener = clickListener;
        item_container = (LinearLayout) itemView.findViewById(R.id.list_item_container);
        title = (TextView) itemView.findViewById(R.id.vendor_name);
        vendorIcon = (ImageView) itemView.findViewById(R.id.vendor_type_image);
        setOnclickListener(this.clickListener);
    }

    private void setOnclickListener(final OnItemClickListener clickListener) {
        item_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("card item got clicked");
                clickListener.onItemClick(view, vendorObj, HomeActionType.LISTVIEW);
            }
        });

    }

    public void updateViewHolder(Context context, WeddingVendor vendorObj) {
        this.vendorObj = vendorObj;
        title.setText(vendorObj.getTitle());
        setOnclickListener(clickListener);
    }
}
