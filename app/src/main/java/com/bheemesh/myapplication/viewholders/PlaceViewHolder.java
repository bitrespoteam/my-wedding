package com.bheemesh.myapplication.viewholders;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bheemesh.myapplication.R;
import com.bheemesh.myapplication.beans.BaseItem;
import com.bheemesh.myapplication.beans.Entity;
import com.bheemesh.myapplication.entities.HomeActionType;
import com.bheemesh.myapplication.listeners.OnBaseItemClickListener;
import com.bheemesh.myapplication.listeners.UpdatableViewHolder;

/**
 * Created by bheemesh on 2/1/16.
 */
public class PlaceViewHolder extends RecyclerView.ViewHolder implements UpdatableViewHolder {
    CardView cardItemLayout;
    ImageView isFavouriteImg;
    TextView title;
    TextView subTitle;
    OnBaseItemClickListener clickListener;
    BaseItem placeObj;

    public PlaceViewHolder(View itemView, OnBaseItemClickListener clickListener) {
        super(itemView);
        this.clickListener = clickListener;
        cardItemLayout = (CardView) itemView.findViewById(R.id.cardlist_item);
        title = (TextView) itemView.findViewById(R.id.listitem_name);
        subTitle = (TextView) itemView.findViewById(R.id.listitem_subname);
        isFavouriteImg = (ImageView) itemView.findViewById(R.id.iv_favourite);
        setOnclickListener(null);
    }

    private void setOnclickListener(final OnBaseItemClickListener clickListener) {
        cardItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("card item got clicked");
                clickListener.onItemClick(view, placeObj, HomeActionType.DETAILS);
            }
        });
    }

    @Override
    public void updateViewHolder(Context context, Entity place) {
        this.placeObj = (BaseItem) place;
        title.setText(placeObj.getName());
        subTitle.setText(placeObj.getCity());
        setOnclickListener(clickListener);
    }
}
