/*
 * Copyright (c) 2015 Newshunt. All rights reserved.
 */

package com.bheemesh.myapplication.Utils;

import android.content.Context;
import android.widget.ImageView;

import com.bheemesh.myapplication.listeners.OnImageLoadListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

/**
 * Utility class to manage images using picasso.
 *
 * @author Bheemesh
 */
public class ImageWrapper {

    public static void loadImage(Context context, ImageView imageView, String imageURL) {
        loadImage(context, imageView, imageURL, false);
    }

    public static void loadImage(Context context, ImageView imageView, String imageURL,
                                 boolean disableAnimation) {
        if (!DataUtil.isEmpty(imageURL)) {
            if (disableAnimation) {
                Picasso.with(context).load(imageURL).noFade().tag(context).into(imageView);
            } else {
                Picasso.with(context).load(imageURL).tag(context).into(imageView);
            }
        }
    }

    public static void loadImageForCallBack(Context context, ImageView imageView, String imageURL,
                                            final OnImageLoadListener onImageLoadListener) {
        if (!DataUtil.isEmpty(imageURL)) {
            Picasso.with(context).load(imageURL).noFade().tag(context).into(imageView, new
                    Callback() {
                @Override
                public void onSuccess() {
                    onImageLoadListener.onSuccess();
                }

                @Override
                public void onError() {
                    onImageLoadListener.onError();
                }
            });
        }
    }

    public static void loadImageWithoutDiskCache(Context context, ImageView imageView,
                                                 String imageURL) {
        if (!DataUtil.isEmpty(imageURL)) {
            Picasso.with(context).load(imageURL).tag(context).networkPolicy(NetworkPolicy.NO_STORE).
                    into(imageView);
        }
    }

    public static void setImage(Context context, ImageView imageView, String imageURL,
                                ImageView.ScaleType scaleType) {
        setImage(context, imageView, imageURL, scaleType, false);
    }

    public static void setImage(Context context, ImageView imageView, String imageURL,
                                ImageView.ScaleType scaleType, boolean disableAnimation) {
        imageView.setAdjustViewBounds(true);
        imageView.setScaleType(scaleType);
        loadImage(context, imageView, imageURL, disableAnimation);
    }

    public static void setImageForCallBack(Context context, ImageView imageView, String imageURL,
                                           ImageView.ScaleType scaleType,
                                           OnImageLoadListener onImageLoadListener) {
        imageView.setAdjustViewBounds(true);
        imageView.setScaleType(scaleType);
        loadImageForCallBack(context, imageView, imageURL, onImageLoadListener);
    }

    public static void setCenterCropImage(Context context, ImageView imageView, String imageURL) {
        setCenterCropImage(context, imageView, imageURL, false);
    }

    public static void setCenterCropImage(Context context, ImageView imageView, String imageURL,
                                          boolean disableAnimation) {
        setImage(context, imageView, imageURL, ImageView.ScaleType.CENTER_CROP, disableAnimation);
    }

    public static void setCenterCropImageForCallback(Context context, ImageView imageView, String
            imageURL, OnImageLoadListener onImageLoadListener) {
        setImageForCallBack(context, imageView, imageURL, ImageView.ScaleType.CENTER_CROP,
                onImageLoadListener);
    }

    public static void cancelRequest(Context context, ImageView imageView) {
        Picasso.with(context).cancelRequest(imageView);
    }
}