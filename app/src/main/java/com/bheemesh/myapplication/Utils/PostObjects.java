package com.bheemesh.myapplication.Utils;

import com.bheemesh.myapplication.beans.UserDetails;

import org.json.JSONObject;

/**
 * Created by bheemesh on 14/3/16.
 *
 * @author bheemesh
 */
public class PostObjects {

    public static JSONObject createRegisterPostBody(UserDetails userDetails) {
        JSONObject object = new JSONObject();
        try {
            object.put("username", userDetails.getUserName().trim());
            object.put("password", userDetails.getUserPassword().trim());
            object.put("email", userDetails.getUserEmail().trim());
        } catch (Exception ex) {

        }
        return object;
    }

    public static JSONObject createLoginPostBody(UserDetails userDetails) {
        JSONObject object = new JSONObject();
        try {
            object.put("username", userDetails.getUserName().trim());
            object.put("password", userDetails.getUserPassword().trim());
        } catch (Exception ex) {

        }
        return object;
    }

    public static JSONObject createUserTokenPostBody(UserDetails userDetails) {
        JSONObject object = new JSONObject();
        try {
            object.put("username", "thatzprem");
            object.put("password", "P@ssw0rd");
        } catch (Exception ex) {

        }
        return object;
    }

    public static JSONObject createAgreePostBody(int agreeCount) {
        JSONObject object = new JSONObject();
        try {
            object.put("agrees_count", ""+agreeCount);
        } catch (Exception ex) {

        }
        return object;
    }

    public static JSONObject createDisAgreePostBody(int agreeCount) {
        JSONObject object = new JSONObject();
        try {
            object.put("disagrees_count", ""+agreeCount);
        } catch (Exception ex) {

        }
        return object;
    }

}
