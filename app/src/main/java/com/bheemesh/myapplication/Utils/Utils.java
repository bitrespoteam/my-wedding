package com.bheemesh.myapplication.Utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.util.Patterns;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.List;
import java.util.Locale;

/**
 * Created by bheemesh on 1/1/16.
 */
public class Utils {

    private static Application application;
    private static Context appContext;

    public static Application getApplication() {
        return application;
    }

    public static void setApplication(Application context) {
        application = context;
    }

    public static Context getApplicationContext() {
        return appContext;
    }

    public static void setApplicationContext(Context context) {
        appContext = context;
    }

    // Check if a string is empty or null
    public static boolean isEmpty(String str) {
        return !(str != null && !(str.trim()).equals(Constants.EMPTY_STRING));
    }

    /**
     * method to check email and redirecting user to next screen
     */
    public static int getDpFromPixels(int pixel, Context context) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pixel / scale);
    }

    public static boolean isEmptyWithoutTrim(String str) {
        if (str != null && (str.length() > 0)) {
            return false;
        }
        return true;
    }

    /**
     * method to check email and redirecting user to next screen
     */
    public static boolean validateEmailAddress(String input) {
        return Patterns.EMAIL_ADDRESS.matcher(input).matches();
    }

    /**
     * This method convets dp unit to equivalent device specific value in
     * pixels.
     */
    public static int getPixelFromDP(int dp, Context context) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale);
    }

    public static int getDeviceScreenWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static int getDeviceScreenHeight(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static Drawable getDrawable(Context context, int resId) {
        if (Build.VERSION.SDK_INT < 21) {
            return context.getResources().getDrawable(resId);
        } else {
            return context.getDrawable(resId);
        }
    }

    public static boolean isAppInstalled(Activity activity, String packageName) {
        try {
            activity.getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        } catch (Exception e) {
            // some other exception and still return false
            return false;
        }
    }

    public static void showKeyBoard(Context context, EditText editText) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService
                (Context
                .INPUT_METHOD_SERVICE);
        editText.requestFocus();
        inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void hideKeyboard(Context context, EditText editText) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService
                (Context
                .INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public static Bitmap decodeFile(String filePath) {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);
        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;
        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, o2);
        return bitmap;
    }

    public static String getLocationAddress(Context context, double latitude, double longitude) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            stringBuilder.append(addresses.get(0).getAddressLine(0));
            stringBuilder.append(", " + addresses.get(0).getAddressLine(1));
            stringBuilder.append(", " + addresses.get(0).getAddressLine(2));
        } catch (Exception e) {

        }
        return  stringBuilder.toString();
    }
}
