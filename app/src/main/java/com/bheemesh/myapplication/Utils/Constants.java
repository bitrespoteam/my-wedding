package com.bheemesh.myapplication.Utils;

/**
 * Created by bheemesh on 1/1/16.
 */
public class Constants {
    public static final String EMPTY_STRING = "";
    public static final String SPACE_STRING = " ";
    public static final String FORWARD_SLASH = "/";
    public static final String EQUAL_CHAR = "=";
    public static final String DOT = ".";
    public static final String EXCLAMATION_MARK = "!";
    public static final String COLLON = ":";
    public static final int SPLASH_TIME = 2000;
    public static final String SEPERATOR = ", ";
    public static final String COMMA_CHARACTER = ",";
    public static final String PIPE_CHARACTER = "|";
    public static final String BOOLEAN_TRUE = "true";
    public static final String BOOLEAN_FALSE = "false";

    public static final String TAB_FAVOURITE = "Favourites";
    public static final String TAB_RECENT = "Recent";
    public static final String TAB_COLLECTION = "Collection";
    public static final String TAB_PROFILE_ABOUT = "About";
    public static final String TAB_PROFILE_GENERAL = "General";
    public static final String TAB_LOCATION = "LOCATION";
    public static final String TAB_INTEREST = "INTEREST";
    public static final String TAB_BY_MARK = "BY MARK";
    public static final String TAB_GENERAL = "General";
    public static final String TAB_ABOUT = "About";
    public static final String TAB_DISPLAY = "Display";
    public static final String TAB_YOUR_VENDOR = "Vendor";
    public static final String TAB_YOUR_CHECKLIST = "Checklist";
    public static final String TAB_YOUR_SHORT_LIST = "Short list";
    public static final String TAB_YOUR_LOVES = "Loved";
    public static final String TAB_YOUR_VENDOR_LIST = "Vendorlist";


    public static final String VIDEO_TYPE = "VIDEO";
    public static final String BUNDLE_ITEM_COLLECTION = "bundle_item_collection";
    public static final String BUNDLE_ITEM = "bundle_item";
    public static final String BUNDLE_WEDDING_ITEM = "bundle_wedding_item";

    // Dummy Data
    public static final String ImageUrl = "http://";
    public static final String state = "http://";
    public static final String district = "http://";
    public static final String taluk = "http://";

    //For TAG
    public static final String TAG_FRAGMENT = "tag fragment";

    //HTTP End points
    public static final String baseUrl = "http://api.newsknit.com/";
    public static final String REGISTER_URL = "user/register";
    public static final String LOGIN_URL = "user/login/";
    public static final String ALL_NEWS_FEED_URL = "feeds/";
    public static final String USER_TOKEN_URL = "api-token-auth/";
    public static final String NEWS_FEED_SELF_URL = "feeds/self/";
    public static final String POST_NEWS_URL = "feeds/";
    public static final String AGREE_URL = "feeds/details/";
    public static final String DISAGREE_URL = "feeds/details/";

    //Handler constants
    public static final int SHOW_TOAST = 501;
    public static final int UPDATE_UI = 502;

    //Error messages
    public static final String UNEXPECTED_ERROR_MESSAGE = "Unexpected error occured.Please try " +
            "after some time";

}
