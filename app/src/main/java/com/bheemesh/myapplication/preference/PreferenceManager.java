package com.bheemesh.myapplication.preference;

import android.content.Context;
import android.content.SharedPreferences;

import com.bheemesh.myapplication.Utils.Constants;
import com.bheemesh.myapplication.Utils.DataUtil;

/**
 * Common class for saving data in preference
 *
 * @author bheemesh.
 */
public class PreferenceManager {

  public static void savePreference(Context context, SavedPreference savedPreference,
                                    Object value) {
    savePreference(context, savedPreference.getPreferenceType(), savedPreference.getName(), value);
  }

  private static void savePreference(Context context, PreferenceType preferenceType,
                                     String key, Object value) {
    String fileName = key;
    if (preferenceType != null) {
      fileName = preferenceType.getFileName();
    }

    SharedPreferences sharedPreferences = context.getSharedPreferences(
        fileName, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPreferences.edit();
    if (value instanceof String) {
      editor.putString(key, value.toString());
    } else if (value instanceof Integer) {
      Integer intValue = DataUtil.parseInt(value.toString(), -1);
      if (intValue != -1) {
        editor.putInt(key, intValue);
      }
    } else if (value instanceof Long) {
      Long longValue = DataUtil.parseLong(value.toString(), -1);
      if (longValue != -1) {
        editor.putLong(key, longValue);
      }
    } else if (value instanceof Boolean) {
      Boolean booleanValue = DataUtil.parseBoolean(value.toString(), null);
      if (booleanValue != null) {
        editor.putBoolean(key, booleanValue);
      }
    }
    editor.commit();
  }

  public static void saveString(Context context, String key, String value) {
    savePreference(context, null, key, value);
  }

  public static void saveInt(Context context, String key, int value) {
    savePreference(context, null, key, value);
  }

  public static void saveLong(Context context, String key, long value) {
    savePreference(context, null, key, value);
  }

  public static void saveBoolean(Context context, String key, boolean value) {
    savePreference(context, null, key, value);
  }

  public static <T> T getPreference(
      Context context, SavedPreference savedPreference, T defaultVal) {
    return getPreference(context, savedPreference.getPreferenceType(), savedPreference.getName(),
        defaultVal);
  }

  public static <T> T getPreference(
      Context context, PreferenceType preferenceType, String key, T defaultVal) {
    String fileName = key;
    if (preferenceType != null) {
      fileName = preferenceType.getFileName();
    }

    SharedPreferences sharedPreferences = context.getSharedPreferences(
        fileName, Context.MODE_PRIVATE);
    if (defaultVal instanceof String) {
      return (T) sharedPreferences.getString(key, defaultVal.toString());

    } else if (defaultVal instanceof Integer) {
      Integer defIntValue = DataUtil.parseInt(defaultVal.toString(), -1);
      Integer intValue = sharedPreferences.getInt(key, defIntValue);
      return (T) intValue;

    } else if (defaultVal instanceof Long) {
      Long defLongValue = DataUtil.parseLong(defaultVal.toString(), -1);
      Long longValue = sharedPreferences.getLong(key, defLongValue);
      return (T) longValue;

    } else if (defaultVal instanceof Boolean) {
      Boolean defBooleanValue = DataUtil.parseBoolean(defaultVal.toString(), null);
      Boolean booleanValue = sharedPreferences.getBoolean(key, defBooleanValue);
      return (T) booleanValue;
    }

    return defaultVal;
  }

  public static String getString(Context context, String key) {
    return getString(context, key, Constants.EMPTY_STRING);
  }

  public static String getString(Context context, String key, String defaultValue) {
    return getPreference(context, null, key, defaultValue);
  }

  public static int getInt(Context context, String key) {
    return getInt(context, key, Integer.MIN_VALUE);
  }

  public static int getInt(Context context, String key, int defaultValue) {
    return getPreference(context, null, key, defaultValue);
  }

  public static long getLong(Context context, String key) {
    return getLong(context, key, Long.MIN_VALUE);
  }

  private static long getLong(Context context, String key, long defaultValue) {
    return getPreference(context, null, key, defaultValue);
  }

  public static boolean getBoolean(Context context, String key, boolean defaultValue) {
    return getPreference(context, null, key, defaultValue);
  }

  public static void remove(Context context, String key) {
    remove(context, key, null);
  }

  public static void remove(Context context, SavedPreference savedPreference) {
    remove(context, savedPreference.getName(), savedPreference.getPreferenceType());
  }

  private static void remove(Context context, String key, PreferenceType preferenceType) {
    String fileName = key;
    if (preferenceType != null) {
      fileName = preferenceType.getFileName();
    }

    SharedPreferences sharedPreferences = context.getSharedPreferences(
        fileName, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPreferences.edit();
    editor.remove(key);
    editor.commit();
  }

}
