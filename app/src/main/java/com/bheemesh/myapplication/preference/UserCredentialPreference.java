/*
 * Copyright (c) 2015 Newshunt. All rights reserved.
 */

package com.bheemesh.myapplication.preference;

/**
 * Helps save app credentials on pref manager.
 *
 * @author bheemesh
 */
public enum UserCredentialPreference implements SavedPreference {
  USER_TOKEN_ID("udId", PreferenceType.USER_TOKEN);

  private String name;
  private PreferenceType preferenceType;

  UserCredentialPreference(String name, PreferenceType preferenceType) {
    this.name = name;
    this.preferenceType = preferenceType;
  }

  @Override
  public PreferenceType getPreferenceType() {
    return preferenceType;
  }

  @Override
  public String getName() {
    return name;
  }
}
