/*
 * Copyright (c) 2015 Newshunt. All rights reserved.
 */

package com.bheemesh.myapplication.preference;

/**
 * Type of preference saved on the app.
 *
 * @author shreyas.desai
 */
public enum PreferenceType {
  USER_TOKEN("appStatePreferences");

  private String fileName;

  PreferenceType(String fileName) {
    this.fileName = fileName;
  }

  public String getFileName() {
    return fileName;
  }
}
