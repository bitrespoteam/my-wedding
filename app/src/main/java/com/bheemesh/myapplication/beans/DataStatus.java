package com.bheemesh.myapplication.beans;

import java.io.Serializable;

/**
 * Created by bheemesh on 5/4/16.
 *
 * @author bheemesh
 */
public class DataStatus implements Serializable {

    private String message;
    private String token;

    public void setMessage(String message) {
        this.message = message;
    }

    public void setToken(String token) {
        this.token = token;
    }

    //--------Getter methods -------

    public String getMessage() {
        return message;
    }

    public String getToken() {
        return token;
    }
}
