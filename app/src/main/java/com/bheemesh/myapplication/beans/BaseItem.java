package com.bheemesh.myapplication.beans;

import com.bheemesh.myapplication.entities.HomeDisplayCardType;
import com.bheemesh.myapplication.entities.VendorType;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bheemesh on 28/11/15.
 *
 * @author bheemesh
 */
public class BaseItem implements Serializable, Entity<String> {
    private String id;
    private VendorType vendorType;
    private String name;
    private String city;
    private String locationId;
    private String numberLikes;
    private String rating;
    private String ratingTotal;
    private String phone;
    private String email;
    private List<VendorPlans> plans;
    private List<VendorSeats> area;
    private List<String> imageUrl;
    private List<Reviews> reviews;
    private HomeDisplayCardType displayCardType;

    public void setVendorType(VendorType vendorType) {
        this.vendorType = vendorType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public void setNumberLikes(String numberLikes) {
        this.numberLikes = numberLikes;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setRatingTotal(String ratingTotal) {
        this.ratingTotal = ratingTotal;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPlans(List<VendorPlans> plans) {
        this.plans = plans;
    }

    public void setArea(List<VendorSeats> area) {
        this.area = area;
    }

    public void setImageUrl(List<String> imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setReviews(List<Reviews> reviews) {
        this.reviews = reviews;
    }

    public void setDisplayCardType(HomeDisplayCardType displayCardType) {
        this.displayCardType = displayCardType;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public void setId(String id) {

    }

    public VendorType getVendorType() {
        return vendorType;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public String getLocationId() {
        return locationId;
    }

    public String getNumberLikes() {
        return numberLikes;
    }

    public String getRating() {
        return rating;
    }

    public String getRatingTotal() {
        return ratingTotal;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public List<VendorPlans> getPlans() {
        return plans;
    }

    public List<VendorSeats> getArea() {
        return area;
    }

    public List<String> getImageUrl() {
        return imageUrl;
    }

    public List<Reviews> getReviews() {
        return reviews;
    }

    public HomeDisplayCardType getDisplayCardType() {
        return displayCardType;
    }
}
