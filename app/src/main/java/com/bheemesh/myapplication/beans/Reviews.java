package com.bheemesh.myapplication.beans;

import java.io.Serializable;

/**
 * Created by bheemesh on 21/08/16.
 */
public class Reviews implements Serializable, Entity<String>{
    private String id;
    private String vendorId;
    private String authorName;
    private String authorImg;
    private String date;
    private String title;
    private String description;
    private String rating;
    private String rating_total;


    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public void setAuthorImg(String authorImg) {
        this.authorImg = authorImg;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setRating_total(String rating_total) {
        this.rating_total = rating_total;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public void setId(String id) {

    }

    public String getVendorId() {
        return vendorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getAuthorImg() {
        return authorImg;
    }

    public String getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getRating() {
        return rating;
    }

    public String getRating_total() {
        return rating_total;
    }
}
