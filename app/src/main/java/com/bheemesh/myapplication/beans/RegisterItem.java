package com.bheemesh.myapplication.beans;

/**
 * Created by bheemesh on 9/3/16.
 *
 * @author bheemesh
 */
public class RegisterItem {
    private String username;
    private String password;

    @Override
    public String toString() {
        return "register[username = " + username + ", password = " + password + "]";
    }
}

