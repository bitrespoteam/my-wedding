package com.bheemesh.myapplication.beans;

import com.bheemesh.myapplication.entities.LoginType;

import java.io.Serializable;

/**
 * Created by bheemesh on 10/1/16.
 *
 * @author bheemesh
 */
public class UserDetails implements Serializable {
    private String userName = null;
    private String userPassword = null;
    private String userEmail = null;
    private String userAbout = null;
    private String userToken = "69c883f304207cb5adb50e5529e749badce247d5";
    private LoginType loginType = LoginType.NONE;

    // Getter methods
    public String getUserToken() {
        return userToken;
    }

    // Setter methods
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public LoginType getLoginType() {
        return loginType;
    }

    public void setLoginType(LoginType loginType) {
        this.loginType = loginType;
    }

    public String getUserAbout() {
        return userAbout;
    }

    public void setUserAbout(String userAbout) {
        this.userAbout = userAbout;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
}
