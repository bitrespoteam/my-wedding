package com.bheemesh.myapplication.beans;

/**
 * Created by bheemesh on 23/4/16.
 *
 * @author bheemesh
 */
public class Status {

    //Represents the application specific error code
    private String code;
    //Application specific error message
    private String message;
    //Application specific error message description
    private String description;
    //Application specific error code type enum
//    private String codeType;

    public Status(String code, String message, String description){
        this.code = code;
        this.message = message;
        this.description = description;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getDescription() {
        return description;
    }
}
