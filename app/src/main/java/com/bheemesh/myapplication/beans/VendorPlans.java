package com.bheemesh.myapplication.beans;

import java.io.Serializable;

/**
 * Created by bheemesh on 21/08/16.
 */
public class VendorPlans implements Serializable, Entity<String> {

    private String id;
    private String vendorId;
    private String title;
    private String description;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
     this.id = id;
    }

    public String getVendorId() {
        return vendorId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
