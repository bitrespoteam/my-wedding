package com.bheemesh.myapplication.beans;

import java.io.Serializable;

/**
 * Created by bheemesh on 22/08/16.
 */
public class WeddingVendor implements Serializable, Entity<String> {
    private String id;
    private String title;
    private String imageUrl;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
      this.id = id;
    }
}
