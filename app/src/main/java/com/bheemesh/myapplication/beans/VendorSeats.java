package com.bheemesh.myapplication.beans;

import java.io.Serializable;

/**
 * Created by bheemesh on 21/08/16.
 */
public class VendorSeats implements Serializable, Entity<String>{

    private String id;
    private String vendor_id;
    private String title;
    private String sitting_seats;
    private String floating_seats;

    public void setVendor_id(String vendor_id) {
        this.vendor_id = vendor_id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSitting_seats(String sitting_seats) {
        this.sitting_seats = sitting_seats;
    }

    public void setFloating_seats(String floating_seats) {
        this.floating_seats = floating_seats;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public void setId(String id) {

    }
}
