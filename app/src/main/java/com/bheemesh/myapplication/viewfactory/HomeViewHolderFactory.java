package com.bheemesh.myapplication.viewfactory;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bheemesh.myapplication.R;
import com.bheemesh.myapplication.entities.HomeDisplayCardType;
import com.bheemesh.myapplication.listeners.OnItemClickListener;
import com.bheemesh.myapplication.viewholders.VendorListViewHolder;
import com.bheemesh.myapplication.viewholders.VendorNormalViewHolder;

/**
 * Created by bheemesh on 2/1/16.
 */
public class HomeViewHolderFactory {

    public static RecyclerView.ViewHolder getNewsViewHolder(ViewGroup parent,
                                                            OnItemClickListener
                                                                    viewOnItemClickPlaceListener,
                                                            HomeDisplayCardType homeDisplayCardType) {
        switch (homeDisplayCardType) {
            case NORMAL:
                return new VendorNormalViewHolder(createView(parent, homeDisplayCardType),
                        viewOnItemClickPlaceListener);
            case VENDOR_LIST_DETAIL:
                return new VendorListViewHolder(createView(parent, homeDisplayCardType),
                        viewOnItemClickPlaceListener);
            default:
                return new VendorNormalViewHolder(createView(parent, homeDisplayCardType),
                        viewOnItemClickPlaceListener);
        }
    }

    private static View createView(ViewGroup parent, HomeDisplayCardType homeDisplayCardType) {
        switch (homeDisplayCardType) {
            case NORMAL:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.recyclerlist_item, parent, false);
            case VENDOR_LIST_DETAIL:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.recycler_vendor_list_item, parent, false);
            default:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.recyclerlist_item, parent, false);
        }
    }

}
