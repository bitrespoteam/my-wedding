/*
 * Copyright (c) 2015 Newshunt. All rights reserved.
 */
package com.bheemesh.myapplication.dao;

import com.bheemesh.myapplication.beans.LocationItem;

import java.util.ArrayList;

/**
 * Groups related Dao interface
 *
 * @author bheemesh
 */
public interface DBLocationDao {


    void open();

    void close();

    ArrayList<LocationItem> getLocationItems(String placeString);

    ArrayList<LocationItem> getAllLocationItems();

    LocationItem getLocation(String locationString);

    String updateLocation(LocationItem locationItemObj);

    LocationItem isLocationPresent(LocationItem locationItemObj);
}
