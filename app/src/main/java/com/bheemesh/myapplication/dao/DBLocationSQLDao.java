/*
 * Copyright (c) 2015 Newshunt. All rights reserved.
 */
package com.bheemesh.myapplication.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import com.bheemesh.myapplication.Utils.Constants;
import com.bheemesh.myapplication.Utils.Utils;
import com.bheemesh.myapplication.beans.LocationItem;

import java.util.ArrayList;

/**
 * Groups Dao handling.
 *
 * @author bheemesh on 7/2/2015.
 */
public class DBLocationSQLDao implements DBLocationDao {

    private DBSQLiteHelper dbHelper;
    private SQLiteDatabase database;
    private Context context;

    public DBLocationSQLDao(Context context) {
        this.context = context;
        dbHelper = DBSQLiteHelper.getInstance(context);
    }

    @Override
    public void open() {
        database = dbHelper.getWritableDatabase();
    }

    @Override
    public void close() {
        dbHelper.close();
    }

    @Override
    public ArrayList<LocationItem> getLocationItems(String itemString) {

        Cursor cursor = null;
        ArrayList<LocationItem> locationItemList = new ArrayList<LocationItem>();
        try {
            //TODO(bheemesh) need to change depending on the country/state/village/nearby. remove
            // locationId
            String query = "select * from " + dbHelper.TABLE_LOCATION + " where " + DBSQLiteConstants.LOCATION_ID
                    + " " + "like " + "'%" + itemString + "%'";
//            System.out.println("BHEEM : DBLocationSQLDao : getLocationItems : query : " + query);
            cursor = dbHelper.getReadableDatabase().rawQuery(query, null);
            if (cursor == null || cursor.getCount() <= 0) {
                return null;
            }
//            System.out.println("BHEEM : DBLocationSQLDao : getCount : " + cursor.getCount());
            if (!cursor.moveToFirst())
                cursor.moveToFirst();
            do {
                LocationItem item = getLocationItem(cursor);
                if (item != null) {
                    locationItemList.add(item);
                }
            } while (cursor.moveToNext());
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return locationItemList;
    }

    @Override
    public ArrayList<LocationItem> getAllLocationItems() {
        Cursor cursor = null;
        ArrayList<LocationItem> itemList = new ArrayList<LocationItem>();
        try {
            String query = "select * from " + dbHelper.TABLE_LOCATION;
//            System.out.println("BHEEM : DBLocationSQLDao : getAllLocationItems : query : " + query);
            cursor = dbHelper.getReadableDatabase().rawQuery(query, null);
            if (cursor == null || cursor.getCount() <= 0) {
                return null;
            }
//            System.out.println("BHEEM : DBLocationSQLDao : getCount : " + cursor.getCount());
            if (!cursor.moveToFirst())
                cursor.moveToFirst();
            do {
                LocationItem item = getLocationItem(cursor);
                if (item != null) {
                    itemList.add(item);
                }
            } while (cursor.moveToNext());
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return itemList;
    }

    @Override
    public LocationItem getLocation(String locationId) {
        Cursor cursor = null;
        LocationItem item = null;
        try {
            String query = "select * from " + dbHelper.TABLE_LOCATION + " where " + DBSQLiteConstants.LOCATION_ID
                    + " = '" + locationId + "'";
//            System.out.println("BHEEM : DBLocationSQLDao : getLocationItem : query : " + query);
            cursor = dbHelper.getReadableDatabase().rawQuery(query, null);
            if (cursor == null || cursor.getCount() <= 0) {
                return null;
            }
//            System.out.println("BHEEM : DBLocationSQLDao : getCount : " + cursor.getCount());
            if (!cursor.moveToFirst())
                cursor.moveToFirst();
            do {
                item = getLocationItem(cursor);
                if (item != null) {
                    return item;
                }
            } while (cursor.moveToNext());
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return item;
    }

    @Override
    public String updateLocation(LocationItem locationItem) {
        if (locationItem == null) {
            return Constants.EMPTY_STRING;
        }

        // Both log and lat are not empty then only check locationItem is present or not.
        if (!Utils.isEmpty(locationItem.getLatitude()) && !Utils.isEmpty(locationItem.getLongitude())) {
            LocationItem dbLocationItem = isLocationPresent(locationItem);
            if (dbLocationItem != null) {
                return dbLocationItem.getId();
            }
        }

        ContentValues values = setLocationContentValues(locationItem);
        String insertedId = Constants.EMPTY_STRING;
        try {
            if (database.update(dbHelper.TABLE_LOCATION, values, DBSQLiteConstants.LOCATION_ID + " = '" +
                    locationItem.getId() + "'", null) == 0) {
                long rowId = database.insert(dbHelper.TABLE_LOCATION, null, values);
                insertedId = "" + rowId;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return insertedId;
    }

    @Override
    public LocationItem isLocationPresent(LocationItem locationItemObj) {
        Cursor cursor = null;
        LocationItem item = null;
        try {
            String query = "select * from " + dbHelper.TABLE_LOCATION + " where "
                    + DBSQLiteConstants.LOCATION_LAT + " = '" + locationItemObj.getLatitude() + "' and "
                    + DBSQLiteConstants.LOCATION_LONG + " = '" + locationItemObj.getLongitude() + "'";
//            System.out.println("BHEEM : DBLocationSQLDao : isLocationPresent : query : " + query);
            cursor = dbHelper.getReadableDatabase().rawQuery(query, null);
            if (cursor == null || cursor.getCount() <= 0) {
                return null;
            }
            if (!cursor.moveToFirst())
                cursor.moveToFirst();
            do {
                item = getLocationItem(cursor);
                if (item != null) {
                    return item;
                }
            } while (cursor.moveToNext());
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return item;
    }

    private LocationItem getLocationItem(Cursor cursor) {
        LocationItem locationItem = new LocationItem();
        if (null == cursor || cursor.getCount() <= 0) {
            return locationItem;
        }
        try {
            ContentValues cv = new ContentValues();
            DatabaseUtils.cursorRowToContentValues(cursor, cv);
            locationItem.setId(cv.getAsString(DBSQLiteConstants.LOCATION_ID));
            locationItem.setCountry(cv.getAsString(DBSQLiteConstants.LOCATION_COUNTRY));
            locationItem.setState((cv.getAsString(DBSQLiteConstants.LOCATION_STATE)));
            locationItem.setDistrict(cv.getAsString(DBSQLiteConstants.LOCATION_DISTRICT));
            locationItem.setTaluk(cv.getAsString(DBSQLiteConstants.LOCATION_TALUK));
            locationItem.setLatitude(cv.getAsString(DBSQLiteConstants.LOCATION_LAT));
            locationItem.setLongitude((cv.getAsString(DBSQLiteConstants.LOCATION_LONG)));
            locationItem.setLandMark((cv.getAsString(DBSQLiteConstants.LOCATION_LANDMARK)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return locationItem;
    }

    private ContentValues setLocationContentValues(LocationItem locationItem) {

        ContentValues values = new ContentValues();
        values.put(DBSQLiteConstants.LOCATION_COUNTRY, locationItem.getCountry());
        values.put(DBSQLiteConstants.LOCATION_STATE, locationItem.getState());
        values.put(DBSQLiteConstants.LOCATION_DISTRICT, locationItem.getDistrict());
        values.put(DBSQLiteConstants.LOCATION_TALUK, locationItem.getTaluk());
        values.put(DBSQLiteConstants.LOCATION_LAT, locationItem.getLatitude());
        values.put(DBSQLiteConstants.LOCATION_LONG, locationItem.getLongitude());
        values.put(DBSQLiteConstants.LOCATION_LANDMARK, locationItem.getLandMark());
        return values;
    }
}
