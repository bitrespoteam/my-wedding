/*
 * Copyright (c) 2015 Newshunt. All rights reserved.
 */
package com.bheemesh.myapplication.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author bheemesh
 */
public class DBSQLiteHelper extends SQLiteOpenHelper {
    //---------------- TABLE NAMES -------------------
    public static final String TABLE_BASE_ITEMS = "NEWS";
    public static final String TABLE_LOCATION = "LOCATION";
    public static final String TABLE_TOPIC = "TOPIC";

    public static String SQLITE_DB_NAME = "bheem.vendor";
    public static int SQLITE_DB_VERSION = 1;
    private static volatile DBSQLiteHelper instance;

    private DBSQLiteHelper(Context context) {
        super(context, SQLITE_DB_NAME, null, SQLITE_DB_VERSION);
    }

    public static DBSQLiteHelper getInstance(Context context) {
        if (instance == null) {
            synchronized (DBSQLiteHelper.class) {
                if (instance == null) {
                    instance = new DBSQLiteHelper(context);
                }
            }
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(getNewsTableSQL());
        db.execSQL(getLocationTable());
        db.execSQL(getTopicTable());
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

    }

    private String getNewsTableSQL() {
        return "CREATE TABLE IF NOT EXISTS "
                + TABLE_BASE_ITEMS + "(" + DBSQLiteConstants.VENDOR_ID
                + " integer primary key autoincrement," + DBSQLiteConstants.VENDOR_NAME
                + " text," + DBSQLiteConstants.VENDOR_VENUE_CITY
                + " text," + DBSQLiteConstants.VENDOR_LOCATION_ID
                + " text," + DBSQLiteConstants.VENDOR_LIKES
                + " text," + DBSQLiteConstants.VENDOR_RATING
                + " text," + DBSQLiteConstants.VENDOR_RATING_TOTAL
                + " text," + DBSQLiteConstants.VENDOR_PHONE
                + " text," + DBSQLiteConstants.VENDOR_EMAIL
                + " text," + DBSQLiteConstants.VENDOR_TYPE
                + " text)";
    }

    private String getLocationTable() {
        return "CREATE TABLE IF NOT EXISTS "
                + TABLE_LOCATION + "(" + DBSQLiteConstants.LOCATION_ID
                + " integer primary key autoincrement," + DBSQLiteConstants.LOCATION_COUNTRY
                + " text," + DBSQLiteConstants.LOCATION_STATE
                + " text," + DBSQLiteConstants.LOCATION_DISTRICT
                + " text," + DBSQLiteConstants.LOCATION_TALUK
                + " text," + DBSQLiteConstants.LOCATION_LAT
                + " text," + DBSQLiteConstants.LOCATION_LONG
                + " text," + DBSQLiteConstants.LOCATION_LANDMARK
                + " text)";
    }

    private String getTopicTable() {
        return "CREATE TABLE IF NOT EXISTS "
                + TABLE_TOPIC + "(" + DBSQLiteConstants.TODO_ID
                + " integer primary key autoincrement," + DBSQLiteConstants.TODO_KEY
                + " text," + DBSQLiteConstants.TODO_TITLE
                + " text," + DBSQLiteConstants.TODO_SUB_TITLTE
                + " text," + DBSQLiteConstants.TOPIC_IS_CHECKED
                + " text)";
    }
}
