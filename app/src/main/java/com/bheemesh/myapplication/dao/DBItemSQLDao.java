/*
 * Copyright (c) 2015 Newshunt. All rights reserved.
 */
package com.bheemesh.myapplication.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import com.bheemesh.myapplication.Utils.Constants;
import com.bheemesh.myapplication.beans.BaseItem;

import java.util.ArrayList;

/**
 * Groups Dao handling.
 *
 * @author bheemesh on 7/2/2015.
 */
public class DBItemSQLDao implements DBItemDao {

    private DBSQLiteHelper dbHelper;
    private SQLiteDatabase database;
    private Context context;
    private String baseGroupVersion = Constants.EMPTY_STRING + 0;

    public DBItemSQLDao(Context context) {
        this.context = context;
        dbHelper = DBSQLiteHelper.getInstance(context);
    }

    @Override
    public void open() {
        database = dbHelper.getWritableDatabase();
    }

    @Override
    public void close() {
        dbHelper.close();
    }

    @Override
    public ArrayList<BaseItem> getBaseItems(String itemString) {

        Cursor cursor = null;
        ArrayList<BaseItem> itemsList = new ArrayList<BaseItem>();
        try {
            String query = "select * from " + dbHelper.TABLE_BASE_ITEMS + " where " + DBSQLiteConstants.VENDOR_NAME
                    + " " +
                    "like " + "'%" + itemString + "%'";
//            System.out.println("BHEEM : DatabaseTable : getWordMatches : query : " + query);
            cursor = dbHelper.getReadableDatabase().rawQuery(query, null);
            if (cursor == null || cursor.getCount() <= 0) {
                return null;
            }
            if (!cursor.moveToFirst())
                cursor.moveToFirst();
            do {
                BaseItem item = getBaseItem(cursor);
                if (item != null) {
                    itemsList.add(item);
                }
            } while (cursor.moveToNext());
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return itemsList;
    }

    @Override
    public ArrayList<BaseItem> getAllBaseItems() {
        Cursor cursor = null;
        ArrayList<BaseItem> itemList = new ArrayList<BaseItem>();
        try {
            String query = "select * from " + dbHelper.TABLE_BASE_ITEMS;
//            System.out.println("BHEEM : DatabaseTable : getWordMatches : query : " + query);
            cursor = dbHelper.getReadableDatabase().rawQuery(query, null);
            if (cursor == null || cursor.getCount() <= 0) {
                return null;
            }
            if (!cursor.moveToFirst())
                cursor.moveToFirst();
            do {
                BaseItem item = getBaseItem(cursor);
                if (item != null) {
                    itemList.add(item);
                }
            } while (cursor.moveToNext());
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return itemList;
    }


    @Override
    public void updateNewsItems(String itemVersion, ArrayList<BaseItem> baseItems) {
        BaseItem item;
        for (int i = 0; i < baseItems.size(); i++) {
            item = baseItems.get(i);
            updateNews(item);
        }
    }

    @Override
    public void updateNews(BaseItem baseItem) {
        ContentValues values = setBaseItemContentValues(baseItem);
        try {
            if (database.update(dbHelper.TABLE_BASE_ITEMS, values, DBSQLiteConstants.VENDOR_ID + " = '" +
                    baseItem.getId() + "'", null) == 0) {
                long rowId = database.insert(dbHelper.TABLE_BASE_ITEMS, null, values);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BaseItem getBaseItem(Cursor cursor) {
        BaseItem item = new BaseItem();
        if (null == cursor || cursor.getCount() <= 0) {
            return item;
        }

        try {
            ContentValues cv = new ContentValues();
            DatabaseUtils.cursorRowToContentValues(cursor, cv);
            item.setId(cv.getAsString(DBSQLiteConstants.VENDOR_ID));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return item;
    }

    private ContentValues setBaseItemContentValues(BaseItem baseItem) {

        ContentValues values = new ContentValues();
        values.put(DBSQLiteConstants.VENDOR_ID, baseItem.getId());
        return values;
    }
}
