/*
 * Copyright (c) 2015 Newshunt. All rights reserved.
 */
package com.bheemesh.myapplication.dao;

import com.bheemesh.myapplication.beans.BaseItem;

import java.util.ArrayList;

/**
 * Groups related Dao interface
 *
 * @author bheemesh
 */
public interface DBItemDao {


    void open();

    void close();

    ArrayList<BaseItem> getBaseItems(String placeString);

    ArrayList<BaseItem> getAllBaseItems();

    void updateNewsItems(String newsVersion, ArrayList<BaseItem> baseItems);

    void updateNews(BaseItem newsObj);
}
