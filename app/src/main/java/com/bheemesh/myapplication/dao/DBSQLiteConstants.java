package com.bheemesh.myapplication.dao;

/**
 * Created by bheemesh on 20/1/16.
 *
 * @author bheemesh
 */
public class DBSQLiteConstants {
    // ------ NEWS RELATED VARIABLES --------------

    public static final String VENDOR_ID = "place_id";
    public static final String VENDOR_NAME = "VENDOR_NAME";
    public static final String VENDOR_VENUE_CITY = "VENDOR_VENUE_CITY";
    public static final String VENDOR_LOCATION_ID = "VENDOR_LOCATION_ID";
    public static final String VENDOR_LIKES = "VENDOR_LIKES";
    public static final String VENDOR_RATING = "VENDOR_RATING";
    public static final String VENDOR_RATING_TOTAL = "VENDOR_RATING_TOTAL";
    public static final String VENDOR_PHONE = "VENDOR_PHONE";
    public static final String VENDOR_EMAIL = "VENDOR_EMAIL";
    public static final String VENDOR_TYPE = "VENDOR_TYPE";

//    private String id;
//    private String vendor_name;
//    private String venue_city;
//    Private String location_id;
//    private String number_likes;
//    private String rating;
//    private String rating_total;
//    Private String phone;
//    Private String email;
//    Private Vender_Type  type;
//    private List<Venue_Price> price_plans
//    Private List<Venue_Seats> area;
//    Private List<String> image_url;
//    Private List<Reviews> reviews;

    //--------- LOCATION RELATED VARIABLES --------------------
    public static final String LOCATION_ID = "location_id";
    public static final String LOCATION_COUNTRY = "location_country";
    public static final String LOCATION_STATE = "location_state";
    public static final String LOCATION_DISTRICT = "location_district";
    public static final String LOCATION_TALUK = "location_taluk";
    public static final String LOCATION_LAT = "location_lat";
    public static final String LOCATION_LONG = "location_long";
    public static final String LOCATION_LANDMARK = "location_landmark";

    // ----------- TODOS TABLE FIELDS-------------

    public static final String TODO_ID = "id";
    public static final String TODO_KEY = "topic_key";
    public static final String TODO_TITLE = "topic_display_name";
    public static final String TODO_SUB_TITLTE = "topic_is_optional";
    public static final String TOPIC_IS_CHECKED = "topic_is_selected";
}
