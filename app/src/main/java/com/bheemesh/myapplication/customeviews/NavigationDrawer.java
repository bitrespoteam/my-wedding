package com.bheemesh.myapplication.customeviews;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bheemesh.myapplication.R;
import com.bheemesh.myapplication.activities.ProfileActivity;
import com.bheemesh.myapplication.activities.SettingActivity;
import com.bheemesh.myapplication.activities.SignOnActivity;

/**
 * Created by bheemesh on 26/1/16.
 *
 * @author bheemesh
 */
public class NavigationDrawer extends LinearLayout {

    //------ Top menu items -----------
    private TextView userName;
    private ImageView profileImage;
    private Button signIn;
    private DrawerLayout drawerLayout;

    //------ Mid menu items -----------
    private LinearLayout home, profile, setting;

    public NavigationDrawer(Context context) {
        super(context);
        init();
    }

    public NavigationDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NavigationDrawer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View view = inflate(getContext(), R.layout.navigation_drawer_layout, null);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams
                .MATCH_PARENT);
        view.setLayoutParams(lp);
        addView(view);
        initProfileUI();
        initSettingMenu();
        setClickListeners();
    }

    public void setNavigationDrawer(DrawerLayout drawerLayout) {
        this.drawerLayout = drawerLayout;
    }

    private void initProfileUI() {
        userName = (TextView) findViewById(R.id.tv_user_name);
        profileImage = (ImageView) findViewById(R.id.iv_profile_image);
        signIn = (Button) findViewById(R.id.btn_sign_in_out);

        profileImage.setImageResource(R.drawable.bheemesh);
    }

    private void initSettingMenu() {
        home = (LinearLayout) findViewById(R.id.nv_home);
        profile = (LinearLayout) findViewById(R.id.nv_profile);
        setting = (LinearLayout) findViewById(R.id.nv_setting);

        setItemImages();
        setTitleUI();

        //TODO(bheemesh) Removing some views for delta release.
        home.setVisibility(View.GONE);
        setting.setVisibility(View.GONE);

    }

    private void setClickListeners() {
        signIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SignOnActivity.class);
                callActivity(intent);
            }
        });

        home.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ProfileActivity.class);
                callActivity(intent);
            }
        });

        profile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ProfileActivity.class);
                callActivity(intent);
            }
        });
        setting.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SettingActivity.class);
                callActivity(intent);
            }
        });
    }

    private void callActivity(Intent intent) {
        drawerLayout.closeDrawer(this);
        getContext().startActivity(intent);
    }


    private void setItemImages() {
        ((ImageView) home.findViewById(R.id.iv_item_type)).setImageResource(R.drawable
                .ic_location_pin);
        ((ImageView) profile.findViewById(R.id.iv_item_type)).setImageResource(R.drawable
                .ic_location_pin);
        ((ImageView) setting.findViewById(R.id.iv_item_type)).setImageResource(R.drawable
                .ic_location_pin);
    }

    private void setTitleUI() {
        ((TextView) home.findViewById(R.id.item_name)).setText("Home");
        ((TextView) profile.findViewById(R.id.item_name)).setText("Profile");
        ((TextView) setting.findViewById(R.id.item_name)).setText("Setting");

        ((TextView) home.findViewById(R.id.item_subname)).setText("15");
        ((TextView) profile.findViewById(R.id.item_subname)).setText("24");
        ((TextView) setting.findViewById(R.id.item_subname)).setText("2");

        home.findViewById(R.id.item_subname).setVisibility(View.VISIBLE);
        profile.findViewById(R.id.item_subname).setVisibility(View.VISIBLE);
        setting.findViewById(R.id.item_subname).setVisibility(View.VISIBLE);
    }


}
