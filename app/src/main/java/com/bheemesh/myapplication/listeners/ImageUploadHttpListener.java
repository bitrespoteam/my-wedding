package com.bheemesh.myapplication.listeners;

/**
 * Interface for callback from HTTPEngine
 * Bheemesh
 */

public interface ImageUploadHttpListener {
    void uploadSuccess(String successCode, String msg);

    void uploadFailed(Exception e, String failureMsg);
}
