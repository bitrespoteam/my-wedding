package com.bheemesh.myapplication.listeners;

/**
 * Listener for story page image load event
 *
 * @author bheemesh.
 */
public interface OnImageLoadListener {
  public void onSuccess();

  public void onError();
}
