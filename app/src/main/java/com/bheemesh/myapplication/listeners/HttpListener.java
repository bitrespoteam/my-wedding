package com.bheemesh.myapplication.listeners;

import com.bheemesh.myapplication.http.HttpRequest;

/**
 * Interface for callback from HTTPEngine
 */

public interface HttpListener {
    void handleHttpResponse(HttpRequest httpRequest);

    void handleHttpException(Exception e, HttpRequest httpRequest);
}
