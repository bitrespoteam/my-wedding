package com.bheemesh.myapplication.listeners;

import com.bheemesh.myapplication.entities.SignOnActionType;

/**
 * Created by bheemesh on 7/1/16.
 */
public interface SignOnEventListener {
    void onSignOnAction(SignOnActionType actionType);
}
