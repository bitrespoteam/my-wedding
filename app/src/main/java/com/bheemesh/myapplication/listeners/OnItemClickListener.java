package com.bheemesh.myapplication.listeners;

import android.view.View;

import com.bheemesh.myapplication.beans.BaseItem;
import com.bheemesh.myapplication.beans.Entity;
import com.bheemesh.myapplication.entities.HomeActionType;

/**
 * Created by bheemesh on 2/1/16.
 */
public interface OnItemClickListener {
    void onItemClick(View view, Entity newsObj, HomeActionType actionType);
}
