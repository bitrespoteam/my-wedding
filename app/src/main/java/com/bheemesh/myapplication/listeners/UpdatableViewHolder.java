/*
 * Copyright (c) 2015 Newshunt. All rights reserved.
 */

package com.bheemesh.myapplication.listeners;

import android.content.Context;

import com.bheemesh.myapplication.beans.Entity;


/**
 * Recycler view holder Updater
 * <p>
 *
 * @author bheemesh
 */
public interface UpdatableViewHolder {
    void updateViewHolder(Context context, Entity entityObj);
}
