package com.bheemesh.myapplication.singleton;

import android.content.Context;
import android.content.SharedPreferences;

import com.bheemesh.myapplication.beans.UserDetails;
import com.bheemesh.myapplication.entities.LoginType;
import com.bheemesh.myapplication.entities.RequestActionType;

/**
 * Created by bheemesh on 10/1/16.
 *
 * @author bheemesh
 */
public class SSO {
    private static volatile SSO instance;
    private final UserDetails userDetails = new UserDetails();
    private SSOPresenter ssoPresenter;
    private String LAST_USER_NAME = "last_user_name";
    private String USER_TOKEN_KEY = "user_token";
    private String USER_NAME_KEY = "user_name";
    private String USER_PASSWORD_KEY= "user_password";
    private String USER_EMAIL_KEY= "user_email";

    private String defaultUserName = "test";
    private String defaultUserPassword = "test";
    private String defaultUserToken = "69c883f304207cb5adb50e5529e749badce247d5";

    private SSO() {
        initUserDetails();
        ssoPresenter = new SSOPresenter(this);
        ssoPresenter.loginToServer(RequestActionType.LOGIN);
    }

    public static SSO getInstance() {
        if (instance == null) {
            synchronized (SSO.class) {
                if (instance == null) {
                    instance = new SSO();
                }
            }
        }
        return instance;
    }

    // UserDetails Callback
    public static String getUserToken() {
        return getInstance().userDetails.getUserToken();
    }

    public static void setUserToken(String userID) {
        getInstance().userDetails.setUserToken(userID);
    }

    public static String getUserName() {
        return getInstance().userDetails.getUserName();
    }

    public static void setUserName(String userName) {
        getInstance().userDetails.setUserName(userName);
    }

    public static void setUserEmail(String userName) {
        getInstance().userDetails.setUserName(userName);
    }

    public static String getUserEmail() {
        return getInstance().userDetails.getUserEmail();
    }

    // UserDetails setting

    public static String getUserPassword() {
        return getInstance().userDetails.getUserPassword();
    }

    public static void setUserPassword(String password) {
        getInstance().userDetails.setUserPassword(password);
    }

    public static String getUserAbout() {
        return getInstance().userDetails.getUserAbout();
    }

    public static void setUserAbout(String userAbout) {
        getInstance().userDetails.setUserAbout(userAbout);
    }


    public static LoginType getLoginType() {
        return getInstance().userDetails.getLoginType();
    }

    public static void setLoginType(LoginType loginType) {
        getInstance().userDetails.setLoginType(loginType);
    }

    public void initUserDetails() {
        userDetails.setUserToken(defaultUserToken);
        userDetails.setUserName("test");
        userDetails.setUserPassword("test");
        userDetails.setLoginType(LoginType.EMAIL);
        userDetails.setUserAbout("Please write something about you. like : Pick one specific " +
                "topic, describe it in detail, and use that to introduce yourself. It\\'s better " +
                "to pick one thing and use it to describe yourself in lots of detail, than to " +
                "give someone a big long list of general topics");
    }


    public void saveUserDetails(Context context, UserDetails userDetails){
        updateLastLoginUser(context);
        setUserName(userDetails.getUserName());
        setUserPassword(userDetails.getUserPassword());
        setUserEmail(userDetails.getUserEmail());
        setUserToken(userDetails.getUserToken());

        SharedPreferences sharedPreferences = context.getSharedPreferences(
                userDetails.getUserName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_TOKEN_KEY, userDetails.getUserToken());
        editor.putString(USER_NAME_KEY, userDetails.getUserName());
        editor.putString(USER_PASSWORD_KEY, userDetails.getUserPassword());
        editor.putString(USER_EMAIL_KEY, userDetails.getUserEmail());
        editor.commit();
    }

    public void getUserDetails(Context context, String userName){
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                userName, Context.MODE_PRIVATE);
        userDetails.setUserName(sharedPreferences.getString(USER_NAME_KEY,
                defaultUserName));
        userDetails.setUserPassword(sharedPreferences.getString(USER_PASSWORD_KEY,
                defaultUserPassword));
//        userDetails.setUserToken(sharedPreferences.getString(USER_TOKEN_KEY,
//                defaultUserToken));
        userDetails.setUserEmail(sharedPreferences.getString(USER_EMAIL_KEY,
                defaultUserName));
        userDetails.setUserToken(defaultUserToken);
    }

    public void updateLastLoginUser(Context context){
        //updating the last user
        SharedPreferences last_sharedPreferences = context.getSharedPreferences(
                LAST_USER_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor_last = last_sharedPreferences.edit();
        editor_last.putString(USER_NAME_KEY, userDetails.getUserName());
        editor_last.commit();
    }

    public void getLostUserDetails(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                LAST_USER_NAME, Context.MODE_PRIVATE);
        String lastUserName = sharedPreferences.getString(USER_NAME_KEY,
                defaultUserName);
        getUserDetails(context, lastUserName);
    }

}
