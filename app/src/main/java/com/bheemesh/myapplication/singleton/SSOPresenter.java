package com.bheemesh.myapplication.singleton;

import com.bheemesh.myapplication.Utils.Constants;
import com.bheemesh.myapplication.Utils.PostObjects;
import com.bheemesh.myapplication.entities.RequestActionType;
import com.bheemesh.myapplication.http.HttpEngine;
import com.bheemesh.myapplication.listeners.HttpListener;
import com.bheemesh.myapplication.http.HttpRequest;

/**
 * Created on 26/1/16.
 *
 * @author bheemesh
 */
public class SSOPresenter implements HttpListener {
    SSO ssoInstance;
    RequestActionType requestActionType;

    public SSOPresenter(SSO ssoInstance) {
        this.ssoInstance = ssoInstance;
    }

    public void loginToServer(RequestActionType requestActionType) {
        this.requestActionType = requestActionType;
        String postBody = Constants.EMPTY_STRING;
        String url = Constants.EMPTY_STRING;
        HttpRequest httpRequest = null;

        switch (requestActionType) {
            case LOGIN:
                postBody = PostObjects.createLoginPostBody(null).toString();
                url = Constants.LOGIN_URL;
                break;
            case USER_TOKEN:
                postBody = PostObjects.createUserTokenPostBody(null).toString();
                url = Constants.USER_TOKEN_URL;
                break;
            default:
                return;
        }

        httpRequest = new HttpRequest(url, postBody, this);
        httpRequest.headerParams = true;
        HttpEngine.getInstance().addRequest(httpRequest);
    }



    //TODO(User-token should be saved in shared preference)
    @Override
    public void handleHttpResponse(HttpRequest httpRequest) {
        System.out.println("BHEEM : Present in the handleHttpResponse : " + requestActionType
                .getName());

        if (httpRequest == null || httpRequest.content == null) {
            System.out.println("BHEEM : Present in the handleHttpResponse : Empty response");
        } else {
            System.out.println("BHEEM : Present in the handleHttpResponse : Got response : " + new
                    String(httpRequest.content));
        }
        switch (requestActionType) {
            case LOGIN:
                loginToServer(RequestActionType.USER_TOKEN);
                break;
            case USER_TOKEN:
                saveUserToken();
                break;
        }
    }

    @Override
    public void handleHttpException(Exception e, HttpRequest httpRequest) {
        System.out.println("BHEEM : Present in the handleHttpException : " + e.getMessage() + " " +
                requestActionType.getName());
    }

    private void saveUserToken(){

    }
}
