package com.bheemesh.myapplication.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bheemesh.myapplication.R;
import com.bheemesh.myapplication.listeners.ActivityEventListener;
import com.bheemesh.myapplication.singleton.SSO;

/**
 * Created by bheemesh on 26/11/15.
 */
public class SettingsGeneralFragment extends BaseFragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    private String pageTabName;
    private boolean isVisible;
    private ActivityEventListener parentActivity;
    private LinearLayout userName, email, password, birthday, location;


    public static SettingsGeneralFragment newInstance(String pageTitle) {
        Bundle args = new Bundle();
        args.putString(ARG_PAGE, pageTitle);
        SettingsGeneralFragment fragment = new SettingsGeneralFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getArguments() != null) {
            pageTabName = getArguments().getString(ARG_PAGE);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        parentActivity = (ActivityEventListener) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting_general, container, false);
        userName = (LinearLayout) view.findViewById(R.id.lv_user_name);
        email = (LinearLayout) view.findViewById(R.id.lv_email);
        password = (LinearLayout) view.findViewById(R.id.lv_password);
        birthday = (LinearLayout) view.findViewById(R.id.lv_birthday);
        location = (LinearLayout) view.findViewById(R.id.lv_location);
        prepareView();
        return view;
    }

    private void prepareView() {
        ((ImageView) userName.findViewById(R.id.iv_item_type)).setImageResource(R.drawable.ic_location_pin);
        ((ImageView) email.findViewById(R.id.iv_item_type)).setImageResource(R.drawable.ic_location_pin);
        ((ImageView) password.findViewById(R.id.iv_item_type)).setImageResource(R.drawable.ic_location_pin);
        ((ImageView) birthday.findViewById(R.id.iv_item_type)).setImageResource(R.drawable.ic_location_pin);
        ((ImageView) location.findViewById(R.id.iv_item_type)).setImageResource(R.drawable.ic_location_pin);

        ((TextView) userName.findViewById(R.id.item_name)).setText(SSO.getUserName()+" "+SSO
                .getUserEmail());
        ((TextView) email.findViewById(R.id.item_name)).setText(SSO.getUserName());
        ((TextView) password.findViewById(R.id.item_name)).setText(SSO.getUserPassword());
        ((TextView) birthday.findViewById(R.id.item_name)).setText("Please enter ur Birthday");
        ((TextView) location.findViewById(R.id.item_name)).setText(SSO.getUserAbout());
    }
}
