package com.bheemesh.myapplication.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bheemesh.myapplication.R;
import com.bheemesh.myapplication.Utils.Utils;
import com.bheemesh.myapplication.beans.ApiResponse;
import com.bheemesh.myapplication.beans.DataStatus;
import com.bheemesh.myapplication.beans.UserDetails;
import com.bheemesh.myapplication.entities.RequestActionType;
import com.bheemesh.myapplication.entities.SignOnActionType;
import com.bheemesh.myapplication.listeners.SignOnEventListener;
import com.bheemesh.myapplication.presenters.SignUpFragmentPresenter;
import com.bheemesh.myapplication.singleton.SSO;

/**
 * Created by bheemesh on 5/1/16.
 */
public class SignUpFragment extends BaseFragment {
    private EditText editUseName, editPassword, editEmail, editBirthday;
    private Button joinBtn;
    private TextView signInTv;
    private SignOnEventListener activityListener;
    private SignUpFragmentPresenter fragPresenter;

    public static SignUpFragment newInstance() {
        SignUpFragment fragment = new SignUpFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        try {
            activityListener = (SignOnEventListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement " +
                    "SignOnEventListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        editUseName = (EditText) view.findViewById(R.id.et_user_name);
        editEmail = (EditText) view.findViewById(R.id.et_email);
        editPassword = (EditText) view.findViewById(R.id.et_password);
        editBirthday = (EditText) view.findViewById(R.id.et_birth_date);

        joinBtn = (Button) view.findViewById(R.id.bt_sign_up);
        signInTv = (TextView) view.findViewById(R.id.tv_sign_in);
        fragPresenter = new SignUpFragmentPresenter(this);

        setOnclickListener();
        return view;
    }


    private void setOnclickListener() {
        joinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUserDetails();
//                activityListener.onSignOnAction(SignOnActionType.GO_TO_SIGN_UP_ABOUT_NEXT);
            }
        });
        signInTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityListener.onSignOnAction(SignOnActionType.GO_TO_SIGN_IN);
            }
        });
    }

    private void updateUserDetails() {
        String name = editUseName.getText().toString();
        String email = editEmail.getText().toString();
        String password = editPassword.getText().toString();
//        String birthday = editBirthday.getText().toString();

        if (Utils.isEmpty(name)) {
            showToast("Please enter User name", Toast.LENGTH_LONG);
            return;
        }

        if (Utils.isEmpty(email)) {
            showToast("Please enter email address", Toast.LENGTH_LONG);
            return;
        }

        if (Utils.isEmpty(password)) {
            showToast("Please enter password", Toast.LENGTH_LONG);
            return;
        }

        name = Utils.isEmpty(name) ? "default name change it" : name;
        email = Utils.isEmpty(email) ? "abcd@abcd.com change it" : email;
        password = Utils.isEmpty(password) ? "default password" : password;

        UserDetails userDetails = new UserDetails();
        userDetails.setUserName(name);
        userDetails.setUserPassword(password);
        userDetails.setUserEmail(email);
        fragPresenter.signUpUserToServer(userDetails, RequestActionType.REGISTER);
    }

    public void showToast(final String messageTxt, final int duration) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), messageTxt, duration).show();
            }
        });
    }

    public void onLoginResult(final  UserDetails userDetails, final ApiResponse apiResponse) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(apiResponse.getStatus().equals("200")){
                    activityListener.onSignOnAction(SignOnActionType.GO_TO_SIGN_UP_ABOUT_NEXT);
                    DataStatus dataStatus = (DataStatus)apiResponse.getData();
                    SSO.getInstance().saveUserDetails(getActivity().getApplicationContext(),
                            userDetails);
                } else {
                    DataStatus dataStatus = (DataStatus)apiResponse.getData();
                    showToast(dataStatus.getMessage(), Toast.LENGTH_LONG);
                    activityListener.onSignOnAction(SignOnActionType.GO_TO_HOME_LIST);
                    SSO.getInstance().initUserDetails();
                }
            }
        });
    }


}
