package com.bheemesh.myapplication.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bheemesh.myapplication.R;
import com.bheemesh.myapplication.Utils.Constants;
import com.bheemesh.myapplication.activities.BaseItemDetailsActivity;
import com.bheemesh.myapplication.adapters.VendorListAdapter;
import com.bheemesh.myapplication.beans.BaseItem;
import com.bheemesh.myapplication.beans.BaseItemContainer;
import com.bheemesh.myapplication.beans.Entity;
import com.bheemesh.myapplication.beans.Status;
import com.bheemesh.myapplication.entities.HomeActionType;
import com.bheemesh.myapplication.listeners.ActivityEventListener;
import com.bheemesh.myapplication.listeners.FragmentEventListener;
import com.bheemesh.myapplication.listeners.OnItemClickListener;
import com.bheemesh.myapplication.presenters.MyWeddingListFragmentPresenter;

/**
 * Created by bheemesh on 26/11/15.
 */
public class MyWeddingListFragment extends BaseFragment implements OnItemClickListener,
        FragmentEventListener {
    public static final String ARG_PAGE = "ARG_PAGE";
    private VendorListAdapter adapter;
    public BaseItemContainer vendorsContainer;
    public String pageTabName;
    private boolean isVisible;
    private RecyclerView vendorListContainer;
    private ActivityEventListener parentActivity;
    private MyWeddingListFragmentPresenter fragPresenter;
    //Error-progress container.
    private RelativeLayout errorProgressContainer;
    private ProgressBar progressBar;
    private RelativeLayout errorLayout;

    public static MyWeddingListFragment newInstance(String pageTitle) {
        Bundle args = new Bundle();
        args.putString(ARG_PAGE, pageTitle);
        MyWeddingListFragment fragment = new MyWeddingListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getArguments() != null) {
            pageTabName = getArguments().getString(ARG_PAGE);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        parentActivity = (ActivityEventListener) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_home, container, false);
        vendorListContainer = (RecyclerView) view.findViewById(R.id.dummyfrag_scrollableview);
        // Error-progress layouts
        errorProgressContainer = (RelativeLayout) view.findViewById(R.id.rl_progress_error_layout);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        errorLayout = (RelativeLayout) view.findViewById(R.id.rv_error_layout);

        fragPresenter = new MyWeddingListFragmentPresenter(this);
        showLoading(true);
        fragPresenter.loadBaseItemInDB();
        return view;
    }

    public void initUIAdapter() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showLoading(false);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity()
                        .getBaseContext());
                vendorListContainer.setLayoutManager(linearLayoutManager);
                vendorListContainer.setHasFixedSize(true);
                adapter = new VendorListAdapter(getActivity(), vendorsContainer, MyWeddingListFragment.this);
                vendorListContainer.setAdapter(adapter);
            }
        });
    }

    @Override
    public void onItemClick(View view, Entity placeObj, HomeActionType actionType) {
        Intent intent = null;
        BaseItem item = (BaseItem) placeObj;
        switch (actionType) {
            case DETAILS:
                intent = new Intent(getActivity(), BaseItemDetailsActivity.class);
                intent.putExtra(Constants.BUNDLE_ITEM_COLLECTION, vendorsContainer);
                intent.putExtra(Constants.BUNDLE_ITEM, item);
                break;
            case UPDATEUI:
                updateParentActivity();
                break;
            case LISTVIEW:
            case NONCLICKABLE:
                break;
        }
        System.out.println("BHEEM : OnItem click in HomeFragment");
        if (intent != null) {
            startActivity(intent);
        }
    }

    private void updateParentActivity() {
        parentActivity.updateActivity(true);
    }

    @Override
    public void updateFragment(boolean updateAllFragment) {
        if (adapter != null) {
            fragPresenter.loadBaseItemInDB();
        }
        adapter.setPlaceCollection(vendorsContainer);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean isFragmentVisible() {
        return isVisible;
    }

    public void showLoading(final boolean show) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (show) {
                    vendorListContainer.setVisibility(View.GONE);
                    errorProgressContainer.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.VISIBLE);
                    errorLayout.setVisibility(View.GONE);
                } else {
                    vendorListContainer.setVisibility(View.VISIBLE);
                    errorProgressContainer.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                    errorLayout.setVisibility(View.GONE);
                }
            }
        });
    }

    public void showErrorView(final Status status) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                vendorListContainer.setVisibility(View.GONE);
                errorProgressContainer.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);
                ((TextView) errorLayout.findViewById(R.id.tv_error_description)).setText(status
                        .getDescription());
            }
        });
    }
}
