package com.bheemesh.myapplication.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.bheemesh.myapplication.beans.BaseItem;
import com.bheemesh.myapplication.beans.BaseItemContainer;
import com.bheemesh.myapplication.entities.HomeDisplayCardType;
import com.bheemesh.myapplication.listeners.NewsUpdatableViewHolder;
import com.bheemesh.myapplication.listeners.OnItemClickListener;
import com.bheemesh.myapplication.viewfactory.HomeViewHolderFactory;

import java.util.ArrayList;

/**
 * Created by bheemesh on 26/11/15.
 */
public class VendorListAdapter extends RecyclerView.Adapter {
    BaseItemContainer vendorCollection;
    ArrayList<BaseItem> newsItems;
    Context context;
    OnItemClickListener clickListener;

    public VendorListAdapter(Context context, BaseItemContainer vendorCollection,
                             OnItemClickListener clickListener) {
        this.context = context;
        this.vendorCollection = vendorCollection;
        this.newsItems = vendorCollection.getBaseItems();
        this.clickListener = clickListener;
    }

    public void setPlaceCollection(BaseItemContainer placeCollection) {
        this.vendorCollection = placeCollection;
        this.newsItems = placeCollection.getBaseItems();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        HomeDisplayCardType cardType = HomeDisplayCardType.fromIndex(viewType);
        return HomeViewHolderFactory.getNewsViewHolder(parent, clickListener, cardType);
    }

    @Override
    public int getItemViewType(int position) {
        HomeDisplayCardType displayCardType = getContentItem(position).getDisplayCardType();
        if (displayCardType == null) {
            return -1;
        }
        return displayCardType.getIndex();
    }

    public BaseItem getContentItem(int position) {
        return newsItems.get(position);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BaseItem placeObj = newsItems.get(position);
        NewsUpdatableViewHolder updatableViewHolder = (NewsUpdatableViewHolder) holder;
        updatableViewHolder.updateViewHolder(context, placeObj);
    }

    @Override
    public int getItemCount() {
        return newsItems == null ? 0 : newsItems.size();
    }
}