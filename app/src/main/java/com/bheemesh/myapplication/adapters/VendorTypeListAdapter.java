package com.bheemesh.myapplication.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bheemesh.myapplication.R;
import com.bheemesh.myapplication.beans.WeddingVendor;
import com.bheemesh.myapplication.listeners.OnItemClickListener;
import com.bheemesh.myapplication.viewholders.VendorTypeViewHolder;

import java.util.ArrayList;

/**
 * Created by bheemesh on 26/11/15.
 */
public class VendorTypeListAdapter extends RecyclerView.Adapter {
    ArrayList<WeddingVendor> weddingVendors;
    Context context;
    OnItemClickListener clickListener;

    public VendorTypeListAdapter(Context context, ArrayList<WeddingVendor> vendorCollection,
                                 OnItemClickListener clickListener) {
        this.context = context;
        this.weddingVendors = vendorCollection;
        this.clickListener = clickListener;
    }

    public void setPlaceCollection(ArrayList<WeddingVendor> placeCollection) {
        this.weddingVendors = placeCollection;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vendor_type_list_item,
                parent,false);
        return new VendorTypeViewHolder(view, clickListener);
    }

    public WeddingVendor getContentItem(int position) {
        return weddingVendors.get(position);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        WeddingVendor placeObj = weddingVendors.get(position);
        VendorTypeViewHolder updatableViewHolder = (VendorTypeViewHolder) holder;
        updatableViewHolder.updateViewHolder(context, placeObj);
    }

    @Override
    public int getItemCount() {
        return weddingVendors == null ? 0 : weddingVendors.size();
    }
}