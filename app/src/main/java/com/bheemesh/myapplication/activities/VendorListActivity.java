package com.bheemesh.myapplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.astuetz.PagerSlidingTabStrip;
import com.bheemesh.myapplication.R;
import com.bheemesh.myapplication.Utils.Constants;
import com.bheemesh.myapplication.adapters.MyWeddingTabFragmentAdapter;
import com.bheemesh.myapplication.adapters.VendorListAdapter;
import com.bheemesh.myapplication.beans.WeddingVendor;
import com.bheemesh.myapplication.fragments.BaseFragment;
import com.bheemesh.myapplication.fragments.SignInFragment;
import com.bheemesh.myapplication.fragments.VendorListFragment;
import com.bheemesh.myapplication.listeners.ActivityEventListener;

/**
 * An blueshark full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class VendorListActivity extends AppCompatActivity implements ActivityEventListener {

    private FrameLayout container;
    private RelativeLayout progressErrorContainer;
    private ProgressBar progressBar;
    private RelativeLayout errorContainer;
    private WeddingVendor vendor;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_list);
        toolbar = (Toolbar) findViewById(R.id.htab_toolbar);
        toolbar.setNavigationIcon(R.drawable.arrow_white);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Vendor List");

        container = (FrameLayout) findViewById(R.id.container);
        progressErrorContainer = (RelativeLayout) findViewById(R.id.rv_error_layout);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        errorContainer = (RelativeLayout) findViewById(R.id.rv_error_layout);
        getIntentData();
        showVendorListFragment();

    }

    private void getIntentData(){
        Intent intent = getIntent();
        vendor = (WeddingVendor) intent.getSerializableExtra(Constants.BUNDLE_WEDDING_ITEM);
    }

    public void showVendorListFragment() {
        if (isFinishing()) {
            return;
        }
        VendorListFragment fragment = VendorListFragment.newInstance(Constants
                .TAB_YOUR_VENDOR_LIST, vendor);
        addFragment(fragment);
    }

    private void addFragment(BaseFragment fragment) {
        try {
            getFragmentManager().beginTransaction().replace(R.id.container, fragment,
                    Constants.TAG_FRAGMENT).addToBackStack(Constants.EMPTY_STRING + fragment.getId()).commit();
        } catch (Exception e) {
            close(false);
        }
    }
    public void close(boolean result) {
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void updateActivity(boolean updateAllFragment) {

    }
}
