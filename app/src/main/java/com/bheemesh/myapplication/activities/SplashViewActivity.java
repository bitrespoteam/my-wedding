package com.bheemesh.myapplication.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.bheemesh.myapplication.R;
import com.bheemesh.myapplication.Utils.Constants;
import com.bheemesh.myapplication.Utils.PermissionUtil;
import com.bheemesh.myapplication.Utils.Utils;
import com.bheemesh.myapplication.http.HttpEngine;
import com.bheemesh.myapplication.presenters.SplashActivityPresenter;
import com.bheemesh.myapplication.singleton.SSO;

import java.util.ArrayList;

/**
 * Created by bheemesh on 3/1/16.
 */
public class SplashViewActivity extends AppCompatActivity {
    private SplashActivityPresenter actPresenter;

    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Utils.setApplicationContext(getApplicationContext());
        updateBaseUrl();
        requestPermission();

        actPresenter = new SplashActivityPresenter(this);
//        actPresenter.initDB();
    }

    private void updateBaseUrl() {
        HttpEngine.baseURL = Constants.baseUrl;
    }

    @Override
    protected void onStart() {
        super.onStart();
        callHomeActivity();
    }

    private void callHomeActivity() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SSO.getInstance().getLostUserDetails(getApplicationContext());
//                if(StringUtil.isEmpty(SSO.getUserName()) || SSO.getUserName().equals("test")){
//                    Intent intent = new Intent(SplashViewActivity.this, SignOnActivity.class);
//                    startActivity(intent);
//                } else {
                Intent intent = new Intent(SplashViewActivity.this, HomeActivity.class);
                startActivity(intent);
//                }
                finish();
            }
        }, Constants.SPLASH_TIME);
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermission() {
        if (PermissionUtil.isVersionMarshmallowAndAbove()) {
            ArrayList<String> permissions = new ArrayList<>(0);

            if (!(checkSelfPermission(Manifest.permission.INTERNET) == PackageManager
                    .PERMISSION_GRANTED)) {
                permissions.add(Manifest.permission.INTERNET);
            }

            if (!(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_GRANTED)) {
                permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }

            if (!(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                    PackageManager.PERMISSION_GRANTED)) {
                permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }

            if (!(checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager
                    .PERMISSION_GRANTED)) {
                permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }

            if (!(checkSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager
                    .PERMISSION_GRANTED)) {
                permissions.add(Manifest.permission.ACCESS_NETWORK_STATE);
            }

            if (permissions.size() > 0) {
                String[] permission = new String[permissions.size()];
                permission = permissions.toArray(permission);
                requestPermissions(permission, 1);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[]
            grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
