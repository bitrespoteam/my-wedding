package com.bheemesh.myapplication.activities;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bheemesh.myapplication.R;
import com.bheemesh.myapplication.listeners.ActivityEventListener;
import com.bheemesh.myapplication.singleton.SSO;

/**
 * An blueshark full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class ProfileActivity extends BaseDrawerActivity implements ActivityEventListener {
    private TextView postNews;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        toolbar = (Toolbar) findViewById(R.id.htab_toolbar);
        setNavigationDrawer(toolbar);
        showNavigationDrawer(true);
        setTitle("Profile");

        postNews = (TextView) findViewById(R.id.tv_post);

        SSO.getInstance();

        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.htab_collapse_toolbar);
        collapsingToolbarLayout.setTitleEnabled(false);
        setOnclickListener();
    }



    private void setOnclickListener() {
        postNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPostNews();
            }
        });
    }

    private void callPostNews() {
//        Intent intent = new Intent(this, PostNewsActivity.class);
//        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void updateActivity(boolean updateAllFragment) {
    }

}
