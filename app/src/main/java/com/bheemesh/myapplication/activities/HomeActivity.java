package com.bheemesh.myapplication.activities;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.astuetz.PagerSlidingTabStrip;
import com.bheemesh.myapplication.R;
import com.bheemesh.myapplication.Utils.Constants;
import com.bheemesh.myapplication.adapters.MyWeddingTabFragmentAdapter;
import com.bheemesh.myapplication.listeners.ActivityEventListener;

/**
 * An blueshark full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class HomeActivity extends BaseDrawerActivity implements ActivityEventListener {
    private ViewPager viewPager;
    private MyWeddingTabFragmentAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_new_list);
        showNavigationDrawer(true);

        viewPager = (ViewPager) findViewById(R.id.htab_viewpager);
        setupViewPager(viewPager);

        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(viewPager);
        tabs.setTextColorResource(R.color.color_white);

        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout)
                findViewById(R.id.htab_collapse_toolbar);
        collapsingToolbarLayout.setTitleEnabled(false);
    }

    private void setupViewPager(ViewPager viewPager) {
        String tabTitles[] = new String[]{Constants.TAB_YOUR_VENDOR, Constants.TAB_YOUR_CHECKLIST,
                Constants.TAB_YOUR_SHORT_LIST,Constants.TAB_YOUR_LOVES};
        adapter = new MyWeddingTabFragmentAdapter(getFragmentManager());
        adapter.setTabs(tabTitles);
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void updateActivity(boolean updateAllFragment) {
        if (adapter != null) {
            adapter.updateAllFragments();
        }
    }
}
