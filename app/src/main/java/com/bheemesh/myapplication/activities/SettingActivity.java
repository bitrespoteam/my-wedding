package com.bheemesh.myapplication.activities;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.astuetz.PagerSlidingTabStrip;
import com.bheemesh.myapplication.R;
import com.bheemesh.myapplication.Utils.Constants;
import com.bheemesh.myapplication.adapters.SettingTabFragmentAdapter;
import com.bheemesh.myapplication.listeners.ActivityEventListener;

/**
 * An blueshark full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SettingActivity extends BaseDrawerActivity implements ActivityEventListener {
    private ViewPager viewPager;
    private SettingTabFragmentAdapter adapter;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        toolbar = (Toolbar) findViewById(R.id.htab_toolbar);
        setNavigationDrawer(toolbar);
        showNavigationDrawer(true);

        viewPager = (ViewPager) findViewById(R.id.htab_viewpager);
        setupViewPager(viewPager);

        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(viewPager);

        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.htab_collapse_toolbar);
        collapsingToolbarLayout.setTitleEnabled(false);
    }

    private void setupViewPager(ViewPager viewPager) {
        String tabTitles[] = new String[]{Constants.TAB_GENERAL, Constants.TAB_ABOUT, Constants
                .TAB_DISPLAY};
        adapter = new SettingTabFragmentAdapter(getFragmentManager());
        adapter.setTabs(tabTitles);
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void updateActivity(boolean updateAllFragment) {
        if (adapter != null) {
            adapter.updateAllFragments();
        }
    }
}
